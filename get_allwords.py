import pickle 


#%% 

"""
    Generates our own dictionary of used words from the dataset.
    This, so we can spellCheck later with the words actually used in product descriptions, etc.
"""

if __name__ == '__main__':
    print('Opening products set')
    with open('./dataset/products_preproc.pkl', 'rb') as products_file:
        products = pickle.load(products_file)
 #%%    
    a = {}
    print('Iterating through the list')
    for prod in products:
        c = []
        product_attr = products[prod]['attributes']
        product_description = products[prod]['product_description']
        
        has_attributes = (product_attr != None)*1
        if (has_attributes):
            for z in product_attr:
                for x in product_attr[z]:
                    if x[1] in ['V', 'J', 'N', 'CD']:
                        if x[0] not in a: 
                            a[x[0]] = [1,x[1],prod]
                            c.append(x[0])
                        else: 
                            if x[0] not in c: 
                                c.append(x[0])
                                a[x[0]][0] += 1
        for y in product_description:
            if y[0] in ['V', 'J', 'N', 'CD']:
                if y[0] not in a: 
                    a[y[0]] = [1,y[1],prod]
                    c.append(y[0])
                else: 
                    if y[0] not in c: 
                        c.append(y[0])
                        a[y[0]][0] += 1

    #%%
    with open('dataset/allwords.pkl', 'wb') as f:
        pickle.dump(a, f, pickle.HIGHEST_PROTOCOL)