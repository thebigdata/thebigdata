# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 17:09:15 2016

@author: simon
"""

import pandas as pd
import pickle 
from corrector import correct

print(correct('great'))

train_file = open('./dataset/train_set.pkl', 'rb')
train = pickle.load(train_file) #this is a list of dictionaries
test_file = open('./dataset/test_set.pkl', 'rb')
test = pickle.load(test_file) #this is a list of dictionaries
products_file = open('./dataset/products.pkl', 'rb')
#products = pickle.load(products_file) #this is a dictionary with as keys the product uid and as value a dictionary containing a dictionary of attributes and a product description
d_train = pd.read_csv('./dataset/train.csv', encoding="ISO-8859-1")
d_test = pd.read_csv('./dataset/test.csv', encoding="ISO-8859-1")

#example data retrieval
#print(train[0]) #prints the dictionary of the first entry
#print(train[0]['product_uid']) #prints the product uid of the first entry
#print(products[train[0]['product_uid']]) #prints all info on the first product in the train set
#print(products[train[0]['product_uid']]['product_description']) #prints the product description of the first product in the train set, note the nested indexing
#print(products[train[0]['product_uid']]['attributes'].popitem()) #print the first attribute-value pair of this product

#brands = set()
#
#for item in products:
#    print(item)
#    if products[item]['attributes'] != None:
#        if 'MFG Brand Name' in products[item]['attributes']:
#            brands.add(products[item]['attributes']['MFG Brand Name'].lower())
#            
#brandlist = sorted(brands)
#
#

#relevance = list(range(15))
#
#for k in range(15):
#    tmplist = list()
#    print (k)
#    for i in range(len(d_train)):
#       wordlen = len(d_train['search_term'][i].split())
#       if wordlen == k: tmplist.append(d_train['relevance'][i])
#
#    relevance[k] = tmplist
#    
#rnd_pred = list()
#for i in range(len(d_test)):
#    wordlen = len(d_test['search_term'][i].split())
#    print ([d_test['id'][i], wordlen])
##    rnd_pred.append(random.choice(relevance[wordlen]))
#    rnd_pred.append(np.mean( relevance[wordlen] ))
#    
#print('rest')   
#pd.DataFrame({"id": d_test['id'], "relevance": rnd_pred}).to_csv('submission.csv',index=False)