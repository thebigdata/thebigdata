# -*- coding: utf-8 -*-
"""
USE LIKE THIS:
    from corrector import correct
    print(correct('test'))
    
    taken from https://github.com/mattalcock/blog/blob/master/2012/12/5/python-spell-checker.rst

Created on Wed Feb 24 15:22:59 2016
@author: simon
"""

import re, collections

use_dictionary = "wordsEn.txt"  # any big text file


def words(text): return re.findall('[a-z]+', text.lower())


def train(features):
    model = collections.defaultdict(lambda: 1)
    for f in features:
        model[f] += 1
    return model


NWORDS = train(words(open(use_dictionary).read()))

alphabet = 'qwertyuiopasdfghjklzxcvbnm'


def edits1(word):
    splits = [(word[:i], word[i:]) for i in range(len(word) + 1)]
    deletes = [a + b[1:] for a, b in splits if b]
    transposes = [a + b[1] + b[0] + b[2:] for a, b in splits if len(b) > 1]
    replaces = [a + c + b[1:] for a, b in splits for c in alphabet if b]
    inserts = [a + c + b for a, b in splits for c in alphabet]
    return set(deletes + transposes + replaces + inserts)


def known_edits2(word):
    return set(e2 for e1 in edits1(word) for e2 in edits1(e1) if e2 in NWORDS)


def known(words): return set(w for w in words if w in NWORDS)


def correct(word):
    candidates = known([word]) or known(edits1(word)) or known_edits2(word) or [word]
    return max(candidates, key=NWORDS.get)

def correct_sentence(sentence):
    corrected = str()
    for word in sentence.split():
        corrected = corrected + correct(word) + " "
    return corrected[:-1]
