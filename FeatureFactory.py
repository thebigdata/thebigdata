import numpy as np
import pandas as pd
import time
import pickle 
import re
from Classification import formatTime
import similarity
from check_abbreviation import abbreviations
from operator import itemgetter

"""

Generates Features. See also Feature Overview.csv for more info on what the features are.

"""

def str_common_word(str1, str2):
    """
        How many common words are there between the two strings?
    """
	return sum(int(str2.find(word)>=0) for word in str1.split())
 
def str_bigram(str1, str2):
    """
        How many bigrams are there between the two given strings?
    """
    count = 0
    for gram1 in ngrams(str1,2):
        for gram2 in ngrams(str2,2):
            if gram1 == gram2:
                count += 1
    return count
 
def str_trigram(str1, str2):
    """
        How many trigrams are there between the two given strings?
    """
    count = 0
    for gram1 in ngrams(str1,3):
        for gram2 in ngrams(str2,3):
            if gram1 == gram2:
                count += 1
    return count
    
def tupel2str(listoftupel):
    """
        Concats tuple into a string separated by spaces
    """
    tmplst = [x[0] for x in listoftupel]
    return ' '.join(tmplst)
 
def findWholeWord(w):
    """
        Returns a regex expression that, when called, tries to find specified word
    """
    return re.compile(r'\b({0})\b'.format(w), flags=re.IGNORECASE).search

def ngrams(search_terms, data, n):
    """
        Calculates the ngrams for the given n between the search terms and the data given
        Returns a tuple with the count, the relative indices and the word types
    """
    search_output = []
    data_output = []
    for i in range(len(search_terms)-n+1):
      search_output.append([x for x in search_terms[i:i+n]])
    for i in range(len(data)-n+1):
      data_output.append([x for x in data[i:i+n]])
    count = 0
    stopwords = ['and','the','to','for','a','with','of','in','is','or','this','no','your','xbi','on','use','from','are','that','it','be','can','an','yes','you','up','not','all','any','as','has','will','other','use','more','at','its','may','when','our','also']
    rel_ind = []
    word_types = []
    get_stem = lambda ls: [x[0] for x in ls]
    get_wordtype = lambda ls: [x[1] for x in ls]
    get_relind = lambda ls: [x[2] for x in ls]
    for gram1 in search_output:
        stopw=0
        if n == 1 and get_stem(gram1)[0] in stopwords:
            stopw = 1
            continue
        for gram2 in data_output:
            if get_stem(gram1) == get_stem(gram2):
                count += 1
                rel_ind.append((get_relind(gram1), get_relind(gram2)))
                word_types.append((get_wordtype(gram1), get_wordtype(gram2)))
                if stopw == 1: break
                
    return (count, rel_ind, word_types)
    
def allgram(search_terms, data, n):
    """
        Does an ngram exist with size n?
    """
    search_output=[x[0] for x in search_terms]
    data_output=[x[0] for x in data]
    count = 0
    for gram1 in search_output:
        for gram2 in data_output:
            if gram1 in gram2:
                count += 1
                break
    if count == n: 
        yes = 1
    else:
        yes = 0
    return (yes)
    
def compound_1grams(search_terms, data, allwords, including_partmatches):
    """
        How many 1grams are there between the two given strings when taking into account that a word can be compound?
    """
    n = 1
    search_output = []
    data_output = []
    for i in range(len(search_terms)-n+1):
      search_output.append([x for x in search_terms[i:i+n]])
    for i in range(len(data)-n+1):
      data_output.append([x for x in data[i:i+n]])
      
    count = 0
    rel_ind = []
    get_stem = lambda ls: [x[0] for x in ls]
    get_relind = lambda ls: [x[2] for x in ls]
    for gram1 in search_output:
        gram1_compounds = []
        base = get_stem(gram1)[0]
        for c,x in enumerate (base):
            if c > 1 and len(base)-c >2:
                if base[:c] in allwords and base[c:] in allwords:
                    gram1_compounds.append(base[:c])
                    gram1_compounds.append(base[c:])
        for comp_gram in gram1_compounds:
            for gram2 in data_output:
                if including_partmatches == 1 and comp_gram in get_stem(gram2)[0]:
                    count += 1
                    rel_ind.append((get_relind(gram1), get_relind(gram2)))
                elif comp_gram in get_stem(gram2):
                    count += 1
                    rel_ind.append((get_relind(gram1), get_relind(gram2)))
    return (count, rel_ind)
    

def abbr_ngrams(search_terms, data):
    """
        How many ngrams are there between the search_terms abd the given data?
        Takes into account abbreviations
        Returns a tuple with the count, the relative indices and the word types
    """
    n = 1
    search_output = []
    for i in range(len(search_terms)-n+1):
      search_output.append([x for x in search_terms[i:i+n]])
    count = 0
    
    rel_ind = []
    word_types = []
    get_stem = lambda ls: [x[0] for x in ls]
    #get_wordtype = lambda ls: [x[1] for x in ls]
    get_relind = lambda ls: [x[2] for x in ls]
    for gram1 in search_output:
        if (len(gram1[0][3]) in range(3,5)) and len([item for item in search_terms if item[0] == gram1[0][0] and len(item) >4]) >0:
            abbreviation_num = abbreviations(get_stem(data),gram1[0][3])
            count += abbreviation_num[0]
            for i,x in enumerate(abbreviation_num[1]):
                indices = get_relind([item for item in data if item[0] == x])
                rel_ind.append((get_relind(gram1),[indices[i]]))
                word_types.append((['N'],['N']))
    return (count, rel_ind, word_types)

def rel_ngrams(search_terms, data, n, allwords):
    """
        How many ngrams are there between the search_terms and the data?
        Takes into account abbreviations
        Returns a tuple with the count, the relative indices and the word types
    """
    search_output = []
    data_output = []
    for i in range(len(search_terms)-n+1):
      search_output.append([x for x in search_terms[i:i+n]])
    for i in range(len(data)-n+1):
      data_output.append([x for x in data[i:i+n]])
    count = 0.0
    rel_ind = []
    stopwords = ['and','the','to','for','a','with','of','in','is','or','this','no','your','xbi','on','use','from','are','that','it','be','can','an','yes','you','up','not','all','any','as','has','will','other','use','more','at','its','may','when','our','also']
    word_types = []
    get_stem = lambda ls: [x[0] for x in ls]
    get_wordtype = lambda ls: [x[1] for x in ls]
    get_relind = lambda ls: [x[2] for x in ls]
    for gram1 in search_output:
        if n == 1 and get_stem(gram1)[0] in stopwords:
            continue
        for gram2 in data_output:
            div = 10000
            if get_stem(gram1) == get_stem(gram2):
                for i in range (0,n):
                    if get_stem(gram1)[i] in allwords:
                        div = div - ((allwords[get_stem(gram1)[i]][0])/n)
                        if div > 0.0:
                            count += div
                        rel_ind.append((get_relind(gram1), get_relind(gram2)))
                        word_types.append((get_wordtype(gram1), get_wordtype(gram2)))
    return (count, rel_ind, word_types)

def rel_ind_dist(title, rel_ind, istitle):
    distance = 0
    latest = 0
    rel_ind = sorted(rel_ind, key=itemgetter(1), reverse=True)
    unique = []
    for x in rel_ind:
        if x[0][0] not in unique:
            unique.append(x[0][0])
    if len(unique) ==1 and istitle ==1: 
        return 1
    if len(rel_ind) ==0: 
        return 0
    for x in rel_ind:
        if latest > 0: 
            distance = distance + (1-(latest - x[1][0]))
            if istitle == 1:
                if (1-(latest - x[1][0]))<1-(1/len(rel_ind)):
                    distance = distance-(1-latest)
            else:
                if (1-(latest - x[1][0]))<1-(1/len(rel_ind)):
                    distance = distance-(1-latest)
                else: distance = distance-(1-x[1][0])                
            latest = x[1][0]
        else: 
            latest = x[1][0]
    if len(rel_ind)>1: len_rel_ind = len(rel_ind)-1
    else: len_rel_ind = 1
    return (distance/len_rel_ind)+(1/len(title))

def hash32(value):    
    """
        Returns the hash32 value of given string
    """ 
    return hash(value) & 0xffffffff

#%% Feature Generation

"""
    Feature Generation
"""
    
def generate_features():

    """
        Generates the features.
        See also: Feature Overview.csv for more info
    """

    starttime = time.time()
    print('Opening train set')
    with open('./dataset/train_set_preproc.pkl', 'rb') as train_file:
        train = pickle.load(train_file) #this is a list of dictionaries
    print('Opening test set')
    with open('./dataset/test_set_preproc.pkl', 'rb') as test_file:
        test = pickle.load(test_file) #this is a list of dictionaries
    print('Opening products set')
    with open('./dataset/products_preproc.pkl', 'rb') as products_file:
        products = pickle.load(products_file)
    with open('./dataset/allwords.pkl', 'rb') as allwords_file:
        allwords = pickle.load(allwords_file)
    print('File loading: {0}'.format(formatTime(time.time() - starttime)))
  
    starttime = time.time()
    roundtime = time.time()
    
    cat = train+test
    last_index = len(train)
    relevances = []
    headers=['id','has_attributes','query_length','has_brandname','attr1gram','attr2gram','attr3gram','descr1gram','descr2gram',
    'descr3gram','title1gram','title2gram','title3gram','attr1gram_ratio','attr2gram_ratio','attr3gram_ratio','descr1gram_ratio','descr2gram_ratio','descr3gram_ratio','title1gram_ratio','title2gram_ratio','title3gram_ratio','brandname_present','rel_ind_desc','rel_ind_title','rel_attr1gram','rel_attr2gram','rel_attr3gram','rel_descr1gram','rel_descr2gram',
    'rel_descr3gram','rel_title1gram','rel_title2gram','rel_title3gram','norm_sum_max', 'max_sim', 'norm_sum_sd', 'mean', 'mean_90','attr_abbrgram','title_abbrgram','descr_abbrgram','comp_gram_part','comp_gram','title_comp_gram','title_comp_gram_part','descr_comp_gram','descr_comp_gram_part','descr_allgram', 'title_allgram','desc_length','norm_sum_max_own', 'max_sim_own', 'norm_sum_sd_own', 'mean_own', 'mean_90_own','sim1gram','sim1gram_own','close1gram', 'close1gram_own','rel_ind_dis_title','d2v_google', 'd2v_own', 'd2v_title_google','d2v_title_own', 'norm_sum_max_title', 'max_sim_title', 'norm_sum_sd_title', 'mean_title', 'mean_90_title', 'norm_sum_max_own_title', 'max_sim_own_title', 'norm_sum_sd_own_title', 'mean_own_title', 'mean_90_own_title', 'sim1gram_title', 'sim1gram_own_title', 'close1gram_title', 'close1gram_own_title']
    features = pd.DataFrame(np.zeros([len(cat),len(headers)]),columns=headers)
    word2vec = similarity.Word2Vec()
    print('Iterating through the list')
    for i, query in enumerate(cat):
        norm_sum_max = 0.0
        max_sim = 0.0
        norm_sum_sd = 0.0
        mean = 0.0
        mean_90 = 0.0
        norm_sum_max_own = 0.0
        max_sim_own = 0.0
        norm_sum_sd_own = 0.0
        mean_own = 0.0
        mean_90_own = 0.0
        sim1gram_own = 0
        sim1gram = 0
        close1gram_own = 0
        close1gram = 0
        rel_ind_dis_title = 0
        if (i < last_index):
            relevances.append(query['relevance'])
            
        if i%5000 == 0: 
            rest = (len(cat) - i) / 5000 * (time.time() - roundtime)
            print ('~' + str(formatTime(rest)) + ' remaining ' + str(round((i+1)/len(cat)*100)) + '%')
            roundtime = time.time()
            
        title = query['product_title']
        search = query['search_term']
        uid = query['product_uid']        
        product_attr = products[uid]['attributes']
        product_description = products[uid]['product_description']
        #print(query)
        #print(product_description)
        
        d2v = word2vec.d2v(search,product_description)
        d2v_title = word2vec.d2v(search,title)
        if (d2v):
            d2v_google = d2v[0]
            d2v_own = d2v[1]
        if (d2v_title):
            d2v_title_google = d2v_title[0]
            d2v_title_own = d2v_title[1]
        word2vec_features = word2vec.get_features(search,product_description,['N','J','V','R'])
        if word2vec_features[0]: 
            norm_sum_max = word2vec_features[0][0]
            max_sim = word2vec_features[0][1]
            norm_sum_sd = word2vec_features[0][2]
            mean = word2vec_features[0][3]
            mean_90 = word2vec_features[0][4]
            sim1gram = word2vec_features[0][5]
            close1gram = word2vec_features[0][6]
        if word2vec_features[1]: 
            norm_sum_max_own = word2vec_features[1][0]
            max_sim_own = word2vec_features[1][1]
            norm_sum_sd_own = word2vec_features[1][2]
            mean_own = word2vec_features[1][3]
            mean_90_own = word2vec_features[1][4]
            sim1gram_own = word2vec_features[1][5]
            close1gram_own = word2vec_features[1][6]
            
        word2vec_features_title = word2vec.get_features(search,title,['N','J','V','R'])    
        if word2vec_features[0]: 
            norm_sum_max_title = word2vec_features_title[0][0]
            max_sim_title = word2vec_features_title[0][1]
            norm_sum_sd_title = word2vec_features_title[0][2]
            mean_title = word2vec_features_title[0][3]
            mean_90_title = word2vec_features_title[0][4]
            sim1gram_title = word2vec_features_title[0][5]
            close1gram_title = word2vec_features_title[0][6]
        if word2vec_features[1]: 
            norm_sum_max_own_title = word2vec_features_title[1][0]
            max_sim_own_title = word2vec_features_title[1][1]
            norm_sum_sd_own_title = word2vec_features_title[1][2]
            mean_own_title = word2vec_features_title[1][3]
            mean_90_own_title = word2vec_features_title[1][4]
            sim1gram_own_title = word2vec_features_title[1][5]
            close1gram_own_title = word2vec_features_title[1][6]


            
        
        desc_length = len(product_description)
        has_attributes = (product_attr != None)*1
        query_length = len(search)        

        if(has_attributes):
            has_brandname = ('MFG Brand Name' in list(product_attr.keys()))*1
            attrlist = list(product_attr.values())
            rel_attr1gram = sum([rel_ngrams(search,attr,1, allwords)[0] for attr in attrlist])
            rel_attr2gram = sum([rel_ngrams(search,attr,2, allwords)[0] for attr in attrlist])
            rel_attr3gram = sum([rel_ngrams(search,attr,3, allwords)[0] for attr in attrlist])
            attr1gram = sum([ngrams(search,attr,1 )[0] for attr in attrlist])
            attr1gram_ratio = attr1gram/query_length
            attr2gram = sum([ngrams(search,attr,2 )[0] for attr in attrlist])
            attr2gram_ratio = attr2gram/query_length
            attr3gram = sum([ngrams(search,attr,3 )[0] for attr in attrlist])
            attr3gram_ratio = attr3gram/query_length
            attr_abbrgram = sum([abbr_ngrams(search,attr)[0] for attr in attrlist])
            comp_gram_part = sum([compound_1grams(search,attr,allwords,1 )[0] for attr in attrlist])
            comp_gram = sum([compound_1grams(search,attr,allwords,0 )[0] for attr in attrlist])
        else:
            has_brandname = 0
            attr1gram = 0
            attr2gram = 0
            attr3gram = 0
            
        if (has_brandname):
            brandname_present = 1*(ngrams(search, product_attr['MFG Brand Name'], 1)[0] > 0) #perhaps take n=len(prod_attr)?
        else:
            brandname_present = 0
        
        rel_title1gram = rel_ngrams(search, title, 1, allwords)[0]
        rel_title2gram = rel_ngrams(search, title, 2, allwords)[0]
        rel_title3gram = rel_ngrams(search, title, 3, allwords)[0]
        rel_descr1gram = rel_ngrams(search, product_description, 1, allwords)[0]
        rel_descr2gram = rel_ngrams(search, product_description, 2, allwords)[0]
        rel_descr3gram = rel_ngrams(search, product_description, 3, allwords)[0]
        title1gram1 = ngrams(search, title, 1)
        title1gram = title1gram1[0]
        title1gram_ratio = title1gram / query_length
        title2gram = ngrams(search, title, 2)[0]
        title2gram_ratio = title2gram / query_length
        title3gram = ngrams(search, title, 3)[0]
        title3gram_ratio = title3gram / query_length
        title_allgram = allgram(search, title, len(search))
        descr1gram1 = ngrams(search, product_description, 1)
        descr1gram = descr1gram1[0]
        descr1gram_ratio = descr1gram/query_length
        descr2gram = ngrams(search, product_description, 2)[0]
        descr2gram_ratio = descr2gram/query_length
        descr3gram = ngrams(search, product_description, 3)[0]
        descr3gram_ratio = descr3gram/query_length
        descr_allgram = allgram(search, product_description, len(search))
        title_abbrgram = abbr_ngrams(search, title)[0]
        title_comp_gram_part = compound_1grams(search,title,allwords,1)[0]
        title_comp_gram = compound_1grams(search,title,allwords,0)[0]
        descr_comp_gram_part = compound_1grams(search,product_description,allwords,1)[0]
        descr_comp_gram = compound_1grams(search,product_description,allwords,0)[0]
        descr_abbrgram = abbr_ngrams(search, product_description)[0]
        
        #this will count the relative number of noun/verb/adjective matches in the 1-grams (hypothethis is that nouns and
        #adjectives are most informative)
#        noun1gram_desc = sum([(x[0] == x[1])*1 for x in ngrams(search, product_description, 1)[2] if x[0]==['N']]) / query_length
#        noun1gram_title = sum([(x[0] == x[1])*1 for x in ngrams(search, title, 1)[2] if x[0]==['N']]) / query_length
#        verb1gram_desc = sum([(x[0] == x[1])*1 for x in ngrams(search, product_description, 1)[2] if x[0]==['V']]) / query_length
#        verb1gram_title = sum([(x[0] == x[1])*1 for x in ngrams(search, title, 1)[2] if x[0]==['V']]) / query_length
#        adj1gram_desc = sum([(x[0] == x[1])*1 for x in ngrams(search, product_description, 1)[2] if x[0]==['A']]) / query_length
#        adj1gram_title = sum([(x[0] == x[1])*1 for x in ngrams(search, title, 1)[2] if x[0]==['A']]) / query_length
        
        rel_ind_desc = sum([x[0][0]*x[1][0] for x in descr1gram1[1]])
        rel_ind_title = sum([x[0][0]*x[1][0] for x in title1gram1[1]])
        rel_ind_dis_title = rel_ind_dist(title, title1gram1[1],1)
        #rel_ind_dis_title = rel_ind_dist(product_description, descr1gram[1]) NEEDS FIXING!!
#        features = features.append(pd.DataFrame([[query['id'],has_attributes,query_length,has_brandname,attr1gram,
#                    attr2gram,attr3gram,descr1gram,descr2gram,descr3gram,title1gram,title2gram,title3gram,brandname_present]], 
#                    columns=headers))
#        cols=['id','has_attributes','query_length','has_brandname','attr1gram','attr2gram','attr3gram','descr1gram','descr2gram',
#                 'descr3gram','title1gram','title2gram','title3gram','attr1gram_ratio','attr2gram_ratio','attr3gram_ratio','descr1gram_ratio','descr2gram_ratio','descr3gram_ratio','title1gram_ratio','title2gram_ratio','title3gram_ratio','brandname_present','rel_ind_desc','rel_ind_title','rel_attr1gram','rel_attr2gram','rel_attr3gram','rel_descr1gram','rel_descr2gram',
#                 'rel_descr3gram','rel_title1gram','rel_title2gram','rel_title3gram','norm_sum_max', 'max_sim', 'norm_sum_sd', 'mean', 'mean_90','attr_abbrgram','title_abbrgram','descr_abbrgram','comp_gram_part','comp_gram','title_comp_gram','title_comp_gram_part','descr_comp_gram','descr_comp_gram_part','descr_allgram', 'title_allgram','desc_length']
#        result = [query['id'],has_attributes,query_length,has_brandname,attr1gram, attr2gram,attr3gram,
#        descr1gram,descr2gram,descr3gram,title1gram,title2gram,title3gram,attr1gram_ratio,attr2gram_ratio,attr3gram_ratio,descr1gram_ratio,descr2gram_ratio,descr3gram_ratio,title1gram_ratio,title2gram_ratio,title3gram_ratio,brandname_present,rel_ind_desc,rel_ind_title,rel_attr1gram, rel_attr2gram,rel_attr3gram,
#        rel_descr1gram,rel_descr2gram,rel_descr3gram,rel_title1gram,rel_title2gram,rel_title3gram,norm_sum_max, max_sim, norm_sum_sd, mean, mean_90,attr_abbrgram,title_abbrgram,descr_abbrgram,comp_gram_part,comp_gram,title_comp_gram,title_comp_gram_part,descr_comp_gram,descr_comp_gram_part, descr_allgram, title_allgram,desc_length]
#        
#        for i,y in enumerate(result):
#            print (str(cols[i])+' = '+str(y))
        #print ([query['id'],has_attributes,query_length,has_brandname,attr1gram, attr2gram,attr3gram,
        #descr1gram,descr2gram,descr3gram,title1gram,title2gram,title3gram,attr1gram_ratio,attr2gram_ratio,attr3gram_ratio,descr1gram_ratio,descr2gram_ratio,descr3gram_ratio,title1gram_ratio,title2gram_ratio,title3gram_ratio,brandname_present,rel_ind_desc,rel_ind_title,rel_attr1gram, rel_attr2gram,rel_attr3gram,
        #rel_descr1gram,rel_descr2gram,rel_descr3gram,rel_title1gram,rel_title2gram,rel_title3gram,norm_sum_max, max_sim, norm_sum_sd, mean, mean_90,attr_abbrgram,title_abbrgram,descr_abbrgram,comp_gram_part,comp_gram,title_comp_gram,title_comp_gram_part,descr_comp_gram,descr_comp_gram_part, descr_allgram, title_allgram,desc_length,norm_sum_max_own, max_sim_own, norm_sum_sd_own, mean_own, mean_90_own, sim1gram, sim1gram_own, close1gram, close1gram_own,rel_ind_dis_title, d2v_google, d2v_own, d2v_title_google,d2v_title_own])
        features.iloc[i] = [query['id'],has_attributes,query_length,has_brandname,attr1gram, attr2gram,attr3gram,
        descr1gram,descr2gram,descr3gram,title1gram,title2gram,title3gram,attr1gram_ratio,attr2gram_ratio,attr3gram_ratio,descr1gram_ratio,descr2gram_ratio,descr3gram_ratio,title1gram_ratio,title2gram_ratio,title3gram_ratio,brandname_present,rel_ind_desc,rel_ind_title,rel_attr1gram, rel_attr2gram,rel_attr3gram,
        rel_descr1gram,rel_descr2gram,rel_descr3gram,rel_title1gram,rel_title2gram,rel_title3gram,norm_sum_max, max_sim, norm_sum_sd, mean, mean_90,attr_abbrgram,title_abbrgram,descr_abbrgram,comp_gram_part,comp_gram,title_comp_gram,title_comp_gram_part,descr_comp_gram,descr_comp_gram_part, descr_allgram, title_allgram,desc_length,norm_sum_max_own, max_sim_own, norm_sum_sd_own, mean_own, mean_90_own, sim1gram, sim1gram_own, close1gram, close1gram_own,rel_ind_dis_title, d2v_google, d2v_own, d2v_title_google, d2v_title_own,norm_sum_max_title, max_sim_title, norm_sum_sd_title, mean_title, mean_90_title, norm_sum_max_own_title, max_sim_own_title, norm_sum_sd_own_title, mean_own_title, mean_90_own_title, sim1gram_title, sim1gram_own_title, close1gram_title, close1gram_own_title]

    print('Writing the feature files to disk...')
    X_train = features.iloc[:last_index, :]
    X_test = features.iloc[last_index:, :]
    
    X_train = np.matrix(X_train)
    X_test = np.matrix(X_test)
    relevances = np.array(relevances)
    
    with open('dataset/train_features_x.pkl', 'wb') as f:
        pickle.dump(X_train, f, pickle.HIGHEST_PROTOCOL)
    with open('dataset/train_features_y.pkl', 'wb') as f:
        pickle.dump(relevances, f, pickle.HIGHEST_PROTOCOL)
    with open('dataset/test_features.pkl', 'wb') as f:
        pickle.dump(X_test, f, pickle.HIGHEST_PROTOCOL)
        
    print('Done.')
    print('Duration: {0} sec.'.format(formatTime(time.time() - starttime)))

    
    
if __name__  == '__main__':
    generate_features()