# -*- coding: utf-8 -*-
"""
USAGE: 
from check_abbreviation import abbreviations

abbreviations(tokens, abbr) returns the number of full occurrences of words (tokens) that can be abbreviated by abbr.
Example: abbreviations(['I', 'cant', 'sleep'], 'ics') returns 1
CAPS agnostic

@author: walraaf
"""


def abbreviations(tokens, abbr):
    abbreviations = []
    first_words = []
    for i,word in enumerate(tokens):
        if i > len(tokens)-3:
            break
        fullabbr = ""
        for c,letter in enumerate(abbr):

            if str.lower(list(tokens[i+c])[0]) == str.lower(list(abbr)[c]):
                if c == 0: fullabbr = str(tokens[i+c])
                else: fullabbr = fullabbr + " " + str(tokens[i+c])
                found = 1
            else:
                found = 0
                break
        if found == 1:
            abbreviations.append(fullabbr)
            first_words.append(str(tokens[i+c]))
    return [len(abbreviations), first_words]