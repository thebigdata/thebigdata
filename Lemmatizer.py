from nltk.stem import WordNetLemmatizer
from nltk.corpus import wordnet
import corrector
from nltk import distance as nltkdist # for distance measure
import nltk
import pickle

class Lemmatizer:

    """
        Lemmatizes data according to our own specifications
    """

    def __init__(self):
        self.WL = WordNetLemmatizer()
        self.allwords = pickle.load(open('./dataset/dictionarywords.pkl', 'rb'))
        
    def get_wordnet_pos(self,treebank_tag):
        if treebank_tag.startswith('J'):
            return wordnet.ADJ
        elif treebank_tag.startswith('V'):
            return wordnet.VERB
        elif treebank_tag.startswith('N'):
            return wordnet.NOUN
        elif treebank_tag.startswith('R'):
            return wordnet.ADV
        else:
            return wordnet.NOUN
    
    def simplify_wordtype(self,tag):
        """
            Simplifies wordtypes to their base/fundamental/most useful type.
        """
        if tag.startswith('J'):
            return 'J'
        elif tag.startswith('N'):
            return 'N'
        elif (tag == 'POS'  or  tag == 'PRP'  or  tag == 'PRP$'):
            return 'PR'
        elif tag.startswith('R'):
            return 'R'
        elif (tag.startswith('V')  or  tag == 'MD'):
            return 'V'
        elif tag.startswith('W'):
            return 'W'
        elif (tag == 'CD' or tag == 'LS'):
            return 'CD'
        else:
            return tag
        
        
    def spellCheck(self, s):
        """
            Spellchecks the given string against a dictionary.
            Will choose the best candidate.
        """
        sfix = s
        if self.allwords is not None and s not in self.allwords:
            sfixed = False
            scandidates = corrector.known_edits2(s)
            bestCorrection = 99
            mostused = 1
            for scandidate in scandidates:
                if scandidate in self.allwords:
                    editdist = nltkdist.edit_distance(s, scandidate)
                    if editdist < bestCorrection:
                        bestCorrection = editdist
                        sfix = scandidate
                    if editdist == bestCorrection:
                        if self.allwords[scandidate][0] > mostused:
                            bestCorrection = editdist
                            mostused = self.allwords[scandidate][0]
                            sfix = scandidate
                    sfixed = True
            if not sfixed:
                for scandidate in scandidates:
                    bestCorrection = 99
                    editdist = nltkdist.edit_distance(s, scandidate)
                    if editdist < bestCorrection:
                        bestCorrection = editdist
                        sfix = scandidate   
        return sfix
            
    def get_lemmas_spellcheck (self, text):
        """
            Will lemmatize, with added spellcheck functionality (when needed)
        """
        PT = nltk.pos_tag(text)
        lemmas = []
        for index, tag in enumerate(PT):
            one_lemma = (self.WL.lemmatize(tag[0], self.get_wordnet_pos(tag[1])),
                       self.simplify_wordtype(tag[1]),
                       ((len(text)-index)/len(text)), 
                       tag[0])
            if (one_lemma[1] == 'N' or one_lemma[1] ==  'J' or one_lemma[1] == 'V' or one_lemma[1] == 'A'):
                if not wordnet.synsets(one_lemma[0]):
                    corrected = self.spellCheck(tag[0])
                    one_lemma = (self.WL.lemmatize(corrected, self.get_wordnet_pos(nltk.pos_tag([corrected])[0][1])),
                               self.simplify_wordtype(nltk.pos_tag([corrected])[0][1]),
                               ((len(text)-index)/len(text)), 
                               tag[0], 'corrected')
            lemmas.append(one_lemma)
        return lemmas
        
    def get_lemmas(self, text):
        PT = nltk.pos_tag(text)
        lemmas = [(self.WL.lemmatize(tag[0], self.get_wordnet_pos(tag[1])),
                   self.simplify_wordtype(tag[1]),
                   ((len(text)-index)/len(text)),
                   tag[0]) for index, tag in enumerate(PT)]
        return lemmas

if __name__  == '__main__':
    import tokenizer
    lt = Lemmatizer()
    print(lt.get_lemmas_spellcheck(tokenizer.create_tokens('Rustolum my otr shelv is very super cool for doing your business warking handycap')))