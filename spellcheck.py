import corrector
import pickle
from nltk import distance as nltkdist # for distance measure

allwords = None
with open('./dataset/allwords.pkl', 'rb') as products_file:
    allwords = pickle.load(products_file)

def spellCheck(s, pdesc):
    
    sfix = s
    pdesc = [p[0] for p in pdesc]
    
    if s not in pdesc:
        if allwords is not None and s not in allwords:
            sfixed = False
            scandidates = corrector.known_edits2(s)
            for scandidate in scandidates:
                if scandidate in pdesc:
                    sfix = scandidate
                    sfixed = True
                    break
            if not sfixed:
                for scandidate in scandidates:
                    bestCorrection = 99
                    editdist = nltkdist.edit_distance(s, scandidate)
                    if editdist < bestCorrection:
                        bestCorrection = editdist
                        sfix = scandidate
                        
    return sfix

if __name__ == '__main__':
    s = 'steele'
    pdesc = [('steel',0), ('hoi',0), ('hello',0), ('heeh',0), ('hihi',0), ('hoho',0), ('haihai',0)]
    print(spellCheck(s,pdesc))