# -*- coding: utf-8 -*-
"""
Created on Sun Mar 20 22:05:24 2016

@author: H.G. van den Boorn
"""
import pickle
import numpy as np
from sklearn.ensemble import RandomForestRegressor, BaggingRegressor
import pandas as pd
from sklearn.metrics import mean_squared_error
from math import floor
import time, datetime
import sys
from sklearn.linear_model import Ridge
import multiprocessing
import psutil
import matplotlib.pyplot as plt
from sklearn.ensemble import AdaBoostRegressor
from sklearn.tree import DecisionTreeRegressor
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import make_scorer, mean_squared_error
#from sklearn.neural_network import MLPRegressor #warning: needed for MLPRegressor, but requires bleeding edge sklearn: version 0.18

#%% File loading

"""
    File and Features
"""

def load_files():
    """
        load_files loads the train and test files.
    """
    print('Loading files...')
    sys.stdout.flush()
    train_file = open('./dataset/train_features_x.pkl', 'rb')
    train_x = pickle.load(train_file)
    train_file = open('./dataset/train_features_y.pkl', 'rb')
    train_y = pickle.load(train_file) 
    test_file = open('./dataset/test_features.pkl', 'rb')
    test = pickle.load(test_file)
    
    print('Done.')
    sys.stdout.flush()
    return (train_x,train_y,test)

def binary(features):
    """
        binary(features) transforms the given features to binary or 0-1 cont. input for NN  
    """
    headers = ['id','has_attributes','length_one','length_two','length_three','length_fourplus','attr1gram','attr2gram','attr3gram','descr1gram','descr2gram',
    'descr3gram','title1gram','title2gram','title3gram','brandname_present','multiple_1gram','multiple_2gram','multiple_3gram','multiple_1gramtitle','norm_sum_max', 'max_sim', 'norm_sum_sd', 'mean', 'mean_90']
    binary = pd.DataFrame(np.zeros([len(features),len(headers)]),columns=headers)
    count = 0
    for i in features.iterrows():
        has_attributes = i[1][1]
        brandname_present = i[1][13]
        length_one = 0
        length_two = 0
        length_three = 0 
        length_fourplus = 0
        attr1gram = i[1][4]
        attr2gram = i[1][5]
        attr3gram = i[1][6]
        descr1gram = i[1][7]
        descr2gram = i[1][8]
        descr3gram = i[1][9]
        title1gram = i[1][10]
        title2gram = i[1][11]
        title3gram = i[1][12]
        multiple_1gram = 0
        multiple_2gram = 0
        multiple_3gram = 0
        multiple_1gramtitle = 0
        norm_sum_max = i[1][25]
        max_sim = i[1][26]
        norm_sum_sd = i[1][27] 
        mean = i[1][28]
        mean_90 = i[1][29]

        if (attr1gram*i[1][2]+descr1gram*i[1][2]+title1gram*i[1][2])>3: multiple_1gram = 1
        if (attr2gram*i[1][2]+descr2gram*i[1][2]+title2gram*i[1][2])>1: multiple_2gram = 1
        if (attr3gram*i[1][2]+descr3gram*i[1][2]+title3gram*i[1][2])>1: multiple_3gram = 1
        if (title1gram*i[1][2])>1: multiple_1gramtitle = 1
        
        if i[1][2] == 1: length_one = 1
        if i[1][2] == 2: length_two = 1
        if i[1][2] == 3: length_three = 1
        if i[1][2] > 3: length_fourplus = 1
        if attr1gram >0 : attr1gram = 1
        if attr2gram >0 : attr2gram = 1
        if attr3gram >0 : attr3gram = 1
        if descr1gram >0 : descr1gram = 1
        if descr2gram >0 : descr2gram = 1
        if descr3gram >0 : descr3gram = 1
        if title1gram >0 : title1gram = 1
        if title2gram >0 : title2gram = 1
        if title3gram >0 : title3gram = 1     
        binary.iloc[count] = [binary.iloc[count][1],has_attributes,length_one,length_two,length_three,length_fourplus,attr1gram,attr2gram,attr3gram,descr1gram,descr2gram,
                      descr3gram,title1gram,title2gram,title3gram,brandname_present,multiple_1gram,multiple_2gram,multiple_3gram,multiple_1gramtitle,norm_sum_max, max_sim, norm_sum_sd, mean, mean_90]
        count += 1
    return binary

#%% Classifiers

"""
    Classifiers
"""
 
def ApplyClassifier(classifier,params, X_train, Y_train, X_test, Y_test=None, make_submission=False):
    """
        ApplyClassifier(classifier,params, X_train, Y_train, X_test, Y_test=None, make_submission=False)
        generic classification function
        applies a given classifier on the train data 
        and predicts labels based on the given test data
        if the test labels are given, it will calculate the root mean squared error for checking performance
        if make_submission is true, it will create a submission file for uploading to kaggle

        Arguments:
        - classifier - the classifier function to use as a regressor
        - params - dictionary of (optional) parameters for the classifier
        - X_train - the data used for training the regressor
        - Y_train - the labels of the data
        - X_test - the unlabeled test-data to apply the regressor to
        - Y_test - the labels for the test-data. if not None, it will return the RMSE between predicted labels
        - make_submission is true, it will create a submission file for uploading to kaggle

        Returns:
        if Y_test test is given, it will return the RMSE, else it will return the y_pred labels
        - RMSE(y_pred, Y_test) - the root mean squared error between the predicted and actual test labels
        - y_pred - the predictions of the labels of the test-data
        if make_submission is true, it will also create a submission file for uploading to kaggle
    """

    id_test = X_test[:, 0].astype(np.int)
    X_train = X_train[:,1:]
    X_test = X_test[:,1:]
    y_pred = classifier(params, X_train, Y_train, X_test)
    
    if make_submission:
        pd.DataFrame({"id": id_test, "relevance": y_pred}).to_csv('submission.csv',index=False)
        
    print('Done.')
    sys.stdout.flush()
    if not(Y_test is None):
        return RMSE(y_pred,Y_test)
    return y_pred

def RandomForest(params, X_train, Y_train, X_test):
    """
        RandomForest(params, X_train, Y_train, X_test)
        trains a bagging regressor based on random forest classifiers on the train data 
        and predicts labels based on the given test data
        see also:
        bagging regressor - http://scikit-learn.org/stable/modules/generated/sklearn.ensemble.BaggingRegressor.html
        random forest regressor - http://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestRegressor.html

        Arguments:
        - X_train - the data used for training the regressor
        - Y_train - the labels of the data
        - X_test - the unlabeled test-data to apply the regressor to
        - params - dictionary of (optional) parameters. these include:
            - forest_n_estimators - the number of trees in the forest. default 10
            - bagging_n_estimators - the number of base estimators in the bagging ensamble. default 50
            - forest_max_depth - max depth of the trees in the forest. default 10
            - max_samples - max ratio of samples to use to train each base estimator. default 0.15

        Returns:
        - y_pred - the predictions of the labels of the test-data
    """
    forest_n_estimators=10 if 'forest_n_estimators' not in params else params['forest_n_estimators']
    bagging_n_estimators=50 if 'bagging_n_estimators' not in params else params['bagging_n_estimators']
    forest_max_depth=10 if 'forest_max_depth' not in params else params['forest_max_depth']
    max_samples=0.15 if 'max_samples' not in params else params['max_samples']
    
    print('Training random forest...')
    sys.stdout.flush()
    rf = RandomForestRegressor(n_estimators=forest_n_estimators, max_depth=forest_max_depth)
    clf = BaggingRegressor(rf, n_estimators=bagging_n_estimators, max_samples=max_samples)
    clf.fit(X_train, Y_train)
    print('Done.\nPredicting Y values...')
    sys.stdout.flush()
    y_pred = clf.predict(X_test)
    return y_pred
    
def AdaBoost(params, X_train, Y_train, X_test):
    """
        AdaBoost(params, X_train, Y_train, X_test)
        trains an AdaBoost ensamble on the train data 
        will use a base_estimator as given in the params (default: bagging regressor based on random forest)
        and predicts labels based on the given test data

        Arguments:
        - X_train - the data used for training the regressor
        - Y_train - the labels of the data
        - X_test - the unlabeled test-data to apply the regressor to
        - params - dictionary of (optional) parameters. these include:
            - n_estimators - the maximum number of estimators at which boosting is terminated. default is 20.
            - learning_rate - shrinks the contribution of each classifier by learning rate. default is 1.
            - base_estimator - the base estimator from which the boosted ensamble is built. 
            default is a bagging regressor based on a random forest

        Returns:
        - y_pred - the predictions of the labels of the test-data
    """

    n_estimators=20 if 'n_estimators' not in params else params['n_estimators']
    learning_rate=1 if 'learning_rate' not in params else params['learning_rate']
    rf = RandomForestRegressor(n_estimators=20, max_depth=10)
    base_estimator = BaggingRegressor(rf,n_estimators=50, max_samples=0.15) if 'base_estimator' not in params else params['base_estimator']
    
    print('Training classifier...')
    sys.stdout.flush()
    clf = AdaBoostRegressor(n_estimators=n_estimators, learning_rate=learning_rate, base_estimator=base_estimator)
    clf.fit(X_train, Y_train)
    print('Done.\nPredicting Y values...')
    sys.stdout.flush()
    y_pred = clf.predict(X_test)
    return y_pred    

def ClassifierRidge(params, X_train, Y_train, X_test):
    n_estimators=50 if 'n_estimators' not in params else params['n_estimators']
    max_samples=0.15 if 'max_samples' not in params else params['max_samples']
    alpha=1.0 if 'alpha' not in params else params['alpha']
    
    print('Training classifier...')
    sys.stdout.flush()
    rf = Ridge(alpha=alpha)
    clf = BaggingRegressor(rf, n_estimators=n_estimators, max_samples=max_samples)
    clf.fit(X_train, Y_train)
    print('Done.\nPredicting Y values...')
    sys.stdout.flush()
    y_pred = clf.predict(X_test)
    return y_pred    

#!!! Requires dev version sklearn 0.18, and uncomment import statement at the top!
#def NN_MLPRegressor(X_train, Y_train, X_test, Y_test=None, make_submission=False):
#    print('Converting to binary features..')
#    sys.stdout.flush()
#    id_test = X_test[:, 0].astype(np.int)   
#    X_train = binary(X_train)
#    X_test = binary(X_test)
#    X_train = X_train[:,1:]
#    X_test = X_test[:,1:]
#    print('Training')
#    sys.stdout.flush()
#    
#    clf = MLPRegressor(hidden_layer_sizes=(50),activation='relu',learning_rate_init=0.005,early_stopping=True,max_iter=10000)
#    clf.fit(X_train, Y_train)
#    print('Done.\nPredicting Y values...')
#    sys.stdout.flush()
#    y_pred = clf.predict(X_test)
#    
#    if make_submission:
#        pd.DataFrame({"id": id_test, "relevance": y_pred}).to_csv('submission.csv',index=False)
#        
#    print('Done.')
#    sys.stdout.flush()
#    if not(Y_test is None):
#        return RMSE(y_pred,Y_test)
#    return y_pred
#    

#%% Logistics

"""
    Logistical functions
"""    

def formatTime(seconds):
    """
        formatTime(seconds)
        formats the given seconds into an 'hour, minute, second, millisecond' string

        Arguments:
        - seconds - the amount of seconds the task took to complete

        Returns:
        - t - the string in the format of 'h hour, m minute, s second, m millisecond'
    """
    ms = floor(seconds*1000)
    t = ""
    if ms > 3600000:
        hours = floor(ms / 3600000)
        s =  "s" if hours > 1 else ""
        t += str(hours) + " hour" + s + ", "
        ms = ms % 3600000
    if ms > 60000:
        minutes = floor(ms / 60000)
        s =  "s" if minutes > 1 else ""
        t += str(minutes) + " minute" + s + ", "
        ms = ms % 60000
    if ms > 1000:
        seconds = floor(ms / 1000)
        s =  "s" if seconds > 1 else ""
        t += str(seconds) + " second" + s + ", "
        ms = ms % 1000
    millis = floor(ms)
    s =  "s" if millis > 1 else ""
    t += str(millis) + " millisecond" + s
    return t

#%% Metrics

"""
    Metrics
"""
    
def RMSE(y_pred, y_truth):
    """
        Returns the root mean squared error between the predicted and actual labels of the test data

        Arguments:
        - y_pred - the predicted labels of the test data
        - y_truth - the actual labels of the test data

        Returns:
        - root mean squared error between y_pred and y_truth
    """
    
    return mean_squared_error(y_truth, y_pred)**0.5    
    
def cross_val(X_train,y_train, function, n_folds = 5, randomized = False, **params):
    """

    """

    assert len(y_train) == len(X_train)
    
    ind = np.arange(len(X_train))
    if randomized: np.random.shuffle(ind)   # possibility to randomize split
    ind = np.array_split(ind, n_folds)
    
    errors = list()

    n_workers = multiprocessing.cpu_count() - 0 
    pool = multiprocessing.Pool(n_workers)
    parent = psutil.Process()
    parent.nice(psutil.IDLE_PRIORITY_CLASS)
    for child in parent.children():                 # setting priority to lowest, otherwise your PC becomes a slug
        child.nice(psutil.IDLE_PRIORITY_CLASS)      # IDLE_PRIORITY_CLASS might not be available on Unix
    
    job = [[0]] * n_folds
    for i in range(n_folds):
       print('job: ' + str(i))
       sys.stdout.flush()
       train_ind = np.delete(np.arange(len(X_train)),ind[i])            # take all except for i
       test_ind  = ind[i]                                               # take only i
#       errors.append(cross_val_job(X_train[train_ind,:],X_train[test_ind,:],y_train[train_ind],y_train[test_ind]))
       job[i] = pool.apply_async(ApplyClassifier, (function,params, X_train[train_ind,:],y_train[train_ind],X_train[test_ind,:],y_train[test_ind]))
    
    print('waiting for results...')
    sys.stdout.flush()
    for i in range(n_folds):
        print('wait: ' + str(i))
        errors.append(job[i].get())    # waiting for results to be finished
        
    pool.close()
    pool.join()
    return errors


def fmean_squared_error(ground_truth, predictions):
    fmean_squared_error_ = mean_squared_error(ground_truth, predictions)**0.5
    return fmean_squared_error_

RMSE2 = make_scorer(fmean_squared_error, greater_is_better=False)
        
if __name__  == '__main__':
    (X_train, Y_train, test) = load_files()

    starttime = time.time()
    print('Started training @ '+str(datetime.datetime.now()))
    sys.stdout.flush()
    folds = 8
    #ADA = cross_val(X_train, Y_train, AdaBoost, n_folds=folds, randomized=True)
    #RF = cross_val(X_train, Y_train, RandomForest, n_folds=folds, randomized=True)
#    CR = cross_val(X_train, Y_train, ClassifierRidge, n_folds=folds, randomized=True)
    
    clf = BaggingRegressor(RandomForestRegressor(n_estimators = 400, max_depth = 10))
    param_grid = {'n_estimators': [50,100]}
    model = GridSearchCV(estimator = clf, param_grid = param_grid, n_jobs = 8, cv = 2, verbose = 20, scoring=RMSE2)
    model.fit(X_train, Y_train)
    
    print("Best parameters found by grid search:")
    print(model.best_params_)
    print("Best CV score:")
    print(model.best_score_)
    print(model.best_score_ + 0.47003199274)
    #    
    print('Training time: {0} sec.'.format(formatTime(time.time() - starttime)))    
    
    #fig, axes = plt.subplots()
    #axes.set_ylim([np.min((ADA,RF))-0.01,np.max((ADA,RF))+0.01])
    #axes.set_title(str(folds)+" fold crossvalidation")
    #axes.boxplot([ADA,RF], labels=['AdaBoost', 'Random forest'], showmeans=True)
    #plt.show()
    
#    y_pred = RandomForest(X_train[:35000],Y_train[:35000], X_train[35000:])