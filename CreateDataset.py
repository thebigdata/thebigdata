import csv
import pickle
from itertools import groupby

"""
    Creates the dataset format we will use in all custom scripts.
"""

def create_dataset():
    headers = ["id", "product_uid", "product_title", "search_term", "relevance", "product_description", "attributes"]
    
    print('Creating products dataset...')
    A = []
    with open('dataset/attributes.csv', 'r', encoding='unicode_escape') as csvfile:
        a = csv.DictReader(csvfile)
        for row in a:
            if row['product_uid'] == '':  # skip the first line which contains the headers
                continue
    
            row[headers[1]] = int(row[headers[1]])
            A.append(row)
    
    func = lambda x: x['product_uid']
    data = sorted(A, key=func)
    ATTRIBUTES = dict()
    for key, group in groupby(data, func):
        C = dict()
        for att in group:
            C[att['name']] = att['value']
        ATTRIBUTES[key] = C
    
    PRODUCTS = dict()
    with open('dataset/product_descriptions.csv', 'r', encoding='unicode_escape') as csvfile:
        d = csv.DictReader(csvfile)
        for row in d:
            row[headers[1]] = int(row[headers[1]])
            info = {headers[5]: row[headers[5]], headers[6]: None}
            if row[headers[1]] in ATTRIBUTES:
                info[headers[6]] = ATTRIBUTES[row[headers[1]]]
            PRODUCTS[row[headers[1]]] = info
    
    print('Creating train set...')
    Train = []
    with open('dataset/train.csv', 'r', encoding='unicode_escape') as csvfile:
        d = csv.DictReader(csvfile)
        for row in d:
            if d.line_num == 1:  # skip the first line which contains the headers
                continue
            row[headers[0]] = int(row[headers[0]])
            row[headers[1]] = int(row[headers[1]])
            row[headers[4]] = float(row[headers[4]])
            Train.append(row)
    
    print('Creating test set...')
    Test = []
    with open('dataset/test.csv', 'r', encoding='unicode_escape') as csvfile:
        d = csv.DictReader(csvfile)
        for row in d:
            if d.line_num == 1:  # skip the first line which contains the headers
                continue
            row[headers[0]] = int(row[headers[0]])
            row[headers[1]] = int(row[headers[1]])
            Test.append(row)
    
    with open('dataset/train_set.pkl', 'wb') as f:
        pickle.dump(Train, f, pickle.HIGHEST_PROTOCOL)
    with open('dataset/test_set.pkl', 'wb') as f:
        pickle.dump(Test, f, pickle.HIGHEST_PROTOCOL)
    with open('dataset/products.pkl', 'wb') as f:  # load with P = pickle.load(f)
        pickle.dump(PRODUCTS, f, pickle.HIGHEST_PROTOCOL)
        
    print('Done.')

if __name__  == '__main__':
    create_dataset()