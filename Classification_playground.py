# -*- coding: utf-8 -*-
"""
Created on Sun Mar 20 22:05:24 2016

@author: H.G. van den Boorn
"""
import pickle
import numpy as np
from sklearn.ensemble import RandomForestRegressor, BaggingRegressor
import pandas as pd
from sklearn.metrics import mean_squared_error
from math import floor
import time, datetime
import sys
from sklearn.linear_model import Ridge
import multiprocessing
import psutil
import matplotlib.pyplot as plt
from sklearn.ensemble import AdaBoostRegressor
from sklearn.tree import DecisionTreeRegressor
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import make_scorer, mean_squared_error
#from sklearn.neural_network import MLPRegressor #warning: needed for MLPRegressor, but requires bleeding edge sklearn: version 0.18
import warnings; warnings.filterwarnings("ignore");
import time
start_time = time.time()

import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestRegressor, ExtraTreesRegressor, RandomForestClassifier
#from sklearn import pipeline, model_selection
from sklearn import pipeline, grid_search
#from sklearn.feature_extraction import DictVectorizer
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.pipeline import FeatureUnion
from sklearn.decomposition import TruncatedSVD
#from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import mean_squared_error, make_scorer
#from nltk.metrics import edit_distance
from nltk.stem.porter import *
stemmer = PorterStemmer()
#from nltk.stem.snowball import SnowballStemmer #0.003 improvement but takes twice as long as PorterStemmer
#stemmer = SnowballStemmer('english')
import re
#import enchant
import random
import xgboost as xgb
random.seed(2016)

#%% File loading

"""
    File and Features
"""

def load_files():
    """
        load_files loads the train and test files.
    """
    print('Loading files...')
    sys.stdout.flush()
    train_file = open('./dataset/train_features_x.pkl', 'rb')
    train_x = pickle.load(train_file)
    train_file = open('./dataset/train_features_y.pkl', 'rb')
    train_y = pickle.load(train_file) 
    test_file = open('./dataset/test_features.pkl', 'rb')
    test = pickle.load(test_file)
    train_add_file = open('./dataset/original_features_train.pkl', 'rb')
    train_add = pickle.load(train_add_file) 
    test_add_file = open('./dataset/original_features_test.pkl', 'rb')
    test_add = pickle.load(test_add_file)
    test_file = open('./dataset/y_all.pkl', 'rb')
    y_all = pickle.load(test_file)
    print('Done.')
    sys.stdout.flush()
    return (train_x,train_y,test,y_all,train_add,test_add)
 
def binary(features):
    """
        binary(features) transforms the given features to binary or 0-1 cont. input for NN  
    """
    headers = ['id','has_attributes','length_one','length_two','length_three','length_fourplus','attr1gram','attr2gram','attr3gram','descr1gram','descr2gram',
    'descr3gram','title1gram','title2gram','title3gram','brandname_present','multiple_1gram','multiple_2gram','multiple_3gram','multiple_1gramtitle','norm_sum_max', 'max_sim', 'norm_sum_sd', 'mean', 'mean_90']
    binary = pd.DataFrame(np.zeros([len(features),len(headers)]),columns=headers)
    count = 0
    for i in features.iterrows():
        has_attributes = i[1][1]
        brandname_present = i[1][13]
        length_one = 0
        length_two = 0
        length_three = 0 
        length_fourplus = 0
        attr1gram = i[1][4]
        attr2gram = i[1][5]
        attr3gram = i[1][6]
        descr1gram = i[1][7]
        descr2gram = i[1][8]
        descr3gram = i[1][9]
        title1gram = i[1][10]
        title2gram = i[1][11]
        title3gram = i[1][12]
        multiple_1gram = 0
        multiple_2gram = 0
        multiple_3gram = 0
        multiple_1gramtitle = 0
        norm_sum_max = i[1][25]
        max_sim = i[1][26]
        norm_sum_sd = i[1][27] 
        mean = i[1][28]
        mean_90 = i[1][29]

        if (attr1gram*i[1][2]+descr1gram*i[1][2]+title1gram*i[1][2])>3: multiple_1gram = 1
        if (attr2gram*i[1][2]+descr2gram*i[1][2]+title2gram*i[1][2])>1: multiple_2gram = 1
        if (attr3gram*i[1][2]+descr3gram*i[1][2]+title3gram*i[1][2])>1: multiple_3gram = 1
        if (title1gram*i[1][2])>1: multiple_1gramtitle = 1
        
        if i[1][2] == 1: length_one = 1
        if i[1][2] == 2: length_two = 1
        if i[1][2] == 3: length_three = 1
        if i[1][2] > 3: length_fourplus = 1
        if attr1gram >0 : attr1gram = 1
        if attr2gram >0 : attr2gram = 1
        if attr3gram >0 : attr3gram = 1
        if descr1gram >0 : descr1gram = 1
        if descr2gram >0 : descr2gram = 1
        if descr3gram >0 : descr3gram = 1
        if title1gram >0 : title1gram = 1
        if title2gram >0 : title2gram = 1
        if title3gram >0 : title3gram = 1     
        binary.iloc[count] = [binary.iloc[count][1],has_attributes,length_one,length_two,length_three,length_fourplus,attr1gram,attr2gram,attr3gram,descr1gram,descr2gram,
                      descr3gram,title1gram,title2gram,title3gram,brandname_present,multiple_1gram,multiple_2gram,multiple_3gram,multiple_1gramtitle,norm_sum_max, max_sim, norm_sum_sd, mean, mean_90]
        count += 1
    return binary

#%% Classifiers

"""
    Classifiers
"""

def ApplyClassifier(classifier,params, X_train, Y_train, X_test, Y_test=None, make_submission=False):
    """
        ApplyClassifier(classifier,params, X_train, Y_train, X_test, Y_test=None, make_submission=False)
        generic classification function
        applies a given classifier on the train data 
        and predicts labels based on the given test data
        if the test labels are given, it will calculate the root mean squared error for checking performance
        if make_submission is true, it will create a submission file for uploading to kaggle

        Arguments:
        - classifier - the classifier function to use as a regressor
        - params - dictionary of (optional) parameters for the classifier
        - X_train - the data used for training the regressor
        - Y_train - the labels of the data
        - X_test - the unlabeled test-data to apply the regressor to
        - Y_test - the labels for the test-data. if not None, it will return the RMSE between predicted labels
        - make_submission is true, it will create a submission file for uploading to kaggle

        Returns:
        if Y_test test is given, it will return the RMSE, else it will return the y_pred labels
        - RMSE(y_pred, Y_test) - the root mean squared error between the predicted and actual test labels
        - y_pred - the predictions of the labels of the test-data
        if make_submission is true, it will also create a submission file for uploading to kaggle
    """
    
    X_train = X_train[:,1:]
    X_test = X_test[:,1:]
    y_pred = classifier(params, X_train, Y_train, X_test)
    
    if make_submission:
        id_test = X_test[:, 0].astype(np.int)
        id_test = id_test.reshape(166693)
        id_test = id_test.tolist()
        pd.DataFrame({"id": id_test[0], "relevance": y_pred}).to_csv('submission.csv',index=False)
        
    print('Done.')
    sys.stdout.flush()
    if not(Y_test is None):
        return RMSE(y_pred,Y_test)
    return y_pred

def RandomForest(params, X_train, Y_train, X_test):
    """
        RandomForest(params, X_train, Y_train, X_test)
        trains a bagging regressor based on random forest classifiers on the train data 
        and predicts labels based on the given test data
        see also:
        bagging regressor - http://scikit-learn.org/stable/modules/generated/sklearn.ensemble.BaggingRegressor.html
        random forest regressor - http://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestRegressor.html

        Arguments:
        - X_train - the data used for training the regressor
        - Y_train - the labels of the data
        - X_test - the unlabeled test-data to apply the regressor to
        - params - dictionary of (optional) parameters. these include:
            - forest_n_estimators - the number of trees in the forest. default 10
            - bagging_n_estimators - the number of base estimators in the bagging ensamble. default 50
            - forest_max_depth - max depth of the trees in the forest. default 10
            - max_samples - max ratio of samples to use to train each base estimator. default 0.15

        Returns:
        - y_pred - the predictions of the labels of the test-data
    """

    forest_n_estimators=10 if 'forest_n_estimators' not in params else params['forest_n_estimators']
    bagging_n_estimators=50 if 'bagging_n_estimators' not in params else params['bagging_n_estimators']
    forest_max_depth=10 if 'forest_max_depth' not in params else params['forest_max_depth']
    max_samples=0.15 if 'max_samples' not in params else params['max_samples']
    
    print('Training random forest...')
    sys.stdout.flush()
    rf = RandomForestRegressor(n_estimators=forest_n_estimators, max_depth=forest_max_depth)
    clf = BaggingRegressor(rf, n_estimators=bagging_n_estimators, max_samples=max_samples)
    clf.fit(X_train, Y_train)
    print('Done.\nPredicting Y values...')
    sys.stdout.flush()
    y_pred = clf.predict(X_test)
    return y_pred
    
def AdaBoost(params, X_train, Y_train, X_test):
    """
        AdaBoost(params, X_train, Y_train, X_test)
        trains an AdaBoost ensamble on the train data 
        will use a base_estimator as given in the params (default: bagging regressor based on random forest)
        and predicts labels based on the given test data

        Arguments:
        - X_train - the data used for training the regressor
        - Y_train - the labels of the data
        - X_test - the unlabeled test-data to apply the regressor to
        - params - dictionary of (optional) parameters. these include:
            - n_estimators - the maximum number of estimators at which boosting is terminated. default is 20.
            - learning_rate - shrinks the contribution of each classifier by learning rate. default is 1.
            - base_estimator - the base estimator from which the boosted ensamble is built. 
            default is a bagging regressor based on a random forest

        Returns:
        - y_pred - the predictions of the labels of the test-data
    """

    n_estimators=20 if 'n_estimators' not in params else params['n_estimators']
    learning_rate=1 if 'learning_rate' not in params else params['learning_rate']
    rf = RandomForestRegressor(n_estimators=20, max_depth=10)
    base_estimator = BaggingRegressor(rf,n_estimators=50, max_samples=0.15) if 'base_estimator' not in params else params['base_estimator']
    
    print('Training classifier...')
    sys.stdout.flush()
    clf = AdaBoostRegressor(n_estimators=n_estimators, learning_rate=learning_rate, base_estimator=base_estimator)
    clf.fit(X_train, Y_train)
    print('Done.\nPredicting Y values...')
    sys.stdout.flush()
    y_pred = clf.predict(X_test)
    return y_pred    

def ClassifierRidge(params, X_train, Y_train, X_test):
    """
        ClassifierRidge(params, X_train, Y_train, X_test)
        trains a bagging regressor based on Ridge regression on the train data 
        and predicts labels based on the given test data
        see also:
        http://scikit-learn.org/stable/modules/generated/sklearn.linear_model.Ridge.html#sklearn.linear_model.Ridge

        Arguments:
        - X_train - the data used for training the regressor
        - Y_train - the labels of the data
        - X_test - the unlabeled test-data to apply the regressor to
        - params - dictionary of (optional) parameters. these include:
            - n_estimators - the number of base estimators in the bagging ensamble. default 50
            - max_samples - max ratio of samples to use to train each base estimator. default 0.15
            - alpha - small positive alpha improves reduce the variance of the estimates. default is 1

        Returns:
        - y_pred - the predictions of the labels of the test-data
    """
    n_estimators=50 if 'n_estimators' not in params else params['n_estimators']
    max_samples=0.15 if 'max_samples' not in params else params['max_samples']
    alpha=1.0 if 'alpha' not in params else params['alpha']
    
    print('Training classifier...')
    sys.stdout.flush()
    rf = Ridge(alpha=alpha)
    clf = BaggingRegressor(rf, n_estimators=n_estimators, max_samples=max_samples)
    clf.fit(X_train, Y_train)
    print('Done.\nPredicting Y values...')
    sys.stdout.flush()
    y_pred = clf.predict(X_test)
    return y_pred    

#!!! Requires dev version sklearn 0.18, and uncomment import statement at the top!
def NN_MLPRegressor(params, X_train, Y_train, X_test, Y_test=None, make_submission=False):
    """
        NN_MLPRegressor(params, X_train, Y_train, X_test, Y_test=None, make_submission=False)
        trains a multilayered perceptron on the train data 
        and predicts labels based on the given test data
        see also:
        http://scikit-learn.org/dev/modules/generated/sklearn.neural_network.MLPRegressor.html

        Arguments:
        - X_train - the data used for training the regressor
        - Y_train - the labels of the data
        - X_test - the unlabeled test-data to apply the regressor to
        - params - dictionary of (optional) parameters. these include:
            - hidden_layer_sizes - the number of neurons in the hidden layer
            - learning_rate_init - the initial learning rate. default 0.001
            - early_stopping - if true, it will terminate training when validation score is not improving. default is True
            - activation - activation function for the hidden layer. default is 'relu' (rectified linear unit)
            - max_iter - maximum times the algorithm iterates unless convergence is reached. default is 1000
        - Y_test - the labels for the test-data. if not None, it will return the RMSE between predicted labels
        - make_submission is true, it will create a submission file for uploading to kaggle

        Returns:
        - y_pred - the predictions of the labels of the test-data
    """
    hidden_layer_sizes = 100 if 'hidden_layer_sizes' not in params else params['hidden_layer_sizes']
    learning_rate_init = 0.001 if 'learning_rate_init' not in params else params['learning_rate_init']
    early_stopping = True if 'early_stopping' not in params else params['early_stopping']
    activation = 'relu' if 'activation' not in params else params['activation']
    max_iter = 1000 if 'max_iter' not in params else params['max_iter']
    print('Converting to binary features..')
    sys.stdout.flush()
    id_test = X_test[:, 0].astype(np.int)   
    #X_train = binary(X_train)
    #X_test = binary(X_test)
    X_train = X_train[:,1:]
    X_test = X_test[:,1:]
    print('Training')
    sys.stdout.flush()
    
    clf = MLPRegressor(hidden_layer_sizes=(hidden_layer_sizes),activation=activation,learning_rate_init=learning_rate_init,early_stopping=early_stopping,max_iter=max_iter)
    clf.fit(X_train, Y_train)
    print('Done.\nPredicting Y values...')
    sys.stdout.flush()
    y_pred = clf.predict(X_test)
    
    if make_submission:
        pd.DataFrame({"id": id_test, "relevance": y_pred}).to_csv('submission.csv',index=False)
        
    print('Done.')
    sys.stdout.flush()
    if not(Y_test is None):
        return RMSE(y_pred,Y_test)
    return y_pred

def Bagging_NN(params, X_train, Y_train, X_test):
    bagging_n_estimators=50 if 'bagging_n_estimators' not in params else params['bagging_n_estimators']
    max_samples=0.15 if 'max_samples' not in params else params['max_samples']
    hidden_layer_sizes = 100 if 'hidden_layer_sizes' not in params else params['hidden_layer_sizes']
    learning_rate_init = 0.001 if 'learning_rate_init' not in params else params['learning_rate_init']
    early_stopping = True if 'early_stopping' not in params else params['early_stopping']
    activation = 'relu' if 'activation' not in params else params['activation']
    max_iter = 1000 if 'max_iter' not in params else params['max_iter']
    print('Converting to binary features..')
    sys.stdout.flush()
    #id_test = X_test[:, 0].astype(np.int)   
    #X_train = binary(X_train)
    #X_test = binary(X_test)
    X_train = X_train[:,1:]
    X_test = X_test[:,1:]
    print('Training')
    sys.stdout.flush()
    
    nn = MLPRegressor(hidden_layer_sizes=(hidden_layer_sizes),activation=activation,learning_rate_init=learning_rate_init,early_stopping=early_stopping,max_iter=max_iter)
    sys.stdout.flush()
    clf = BaggingRegressor(nn, n_estimators=bagging_n_estimators, max_samples=max_samples)
    clf.fit(X_train, Y_train)
    print('Done.\nPredicting Y values...')
    sys.stdout.flush()
    y_pred = clf.predict(X_test)
    return y_pred
    
#%% Logistics

"""
    Logistical functions
"""    

def formatTime(seconds):
    """
        formatTime(seconds)
        formats the given seconds into an 'hour, minute, second, millisecond' string

        Arguments:
        - seconds - the amount of seconds the task took to complete

        Returns:
        - t - the string in the format of 'h hour, m minute, s second, m millisecond'
    """
    ms = floor(seconds*1000)
    t = ""
    if ms > 3600000:
        hours = floor(ms / 3600000)
        s =  "s" if hours > 1 else ""
        t += str(hours) + " hour" + s + ", "
        ms = ms % 3600000
    if ms > 60000:
        minutes = floor(ms / 60000)
        s =  "s" if minutes > 1 else ""
        t += str(minutes) + " minute" + s + ", "
        ms = ms % 60000
    if ms > 1000:
        seconds = floor(ms / 1000)
        s =  "s" if seconds > 1 else ""
        t += str(seconds) + " second" + s + ", "
        ms = ms % 1000
    millis = floor(ms)
    s =  "s" if millis > 1 else ""
    t += str(millis) + " millisecond" + s
    return t

#%% Metrics

"""
    Metrics
"""
    
def RMSE2(y_pred, y_truth):
    """
        Returns the root mean squared error between the predicted and actual labels of the test data

        Arguments:
        - y_pred - the predicted labels of the test data
        - y_truth - the actual labels of the test data

        Returns:
        - root mean squared error between y_pred and y_truth
    """
    
    return mean_squared_error(y_truth, y_pred)**0.5    
    
def cross_val(X_train,y_train, function, n_folds = 5, randomized = False, **params):
    """
        Applies cross validation using the given train set.

        Arguments:
        - X_train - the train data
        - y_train - labels of the train data
        - function - 
        - n_folds - how many folds should be used for the cross validation
        - randomized - if true, randomize split of folds
        - params - any other parameters given into this function collected into a dictionary structure

        Returns:
        - errors - 
    """
    assert len(y_train) == len(X_train)
    
    ind = np.arange(len(X_train))
    if randomized: np.random.shuffle(ind)   # possibility to randomize split
    ind = np.array_split(ind, n_folds)
    
    errors = list()
    n_workers = multiprocessing.cpu_count() - 0 
    pool = multiprocessing.Pool(n_workers)
    parent = psutil.Process()
    parent.nice(psutil.IDLE_PRIORITY_CLASS)
    for child in parent.children():                 # setting priority to lowest, otherwise your PC becomes a slug
        child.nice(psutil.IDLE_PRIORITY_CLASS)      # IDLE_PRIORITY_CLASS might not be available on Unix
    
    job = [[0]] * n_folds
    for i in range(n_folds):
       print('job: ' + str(i))
       sys.stdout.flush()
       train_ind = np.delete(np.arange(len(X_train)),ind[i])            # take all except for i
       test_ind  = ind[i]                                               # take only i
       #errors.append(cross_val_job(X_train[train_ind,:],X_train[test_ind,:],y_train[train_ind],y_train[test_ind]))
       job[i] = pool.apply_async(ApplyClassifier, (function,params, X_train[train_ind,:],y_train[train_ind],X_train[test_ind,:],y_train[test_ind]))
    
    print('waiting for results...')
    sys.stdout.flush()
    for i in range(n_folds):
        print('wait: ' + str(i))
        errors.append(job[i].get())    # waiting for results to be finished
        
    pool.close()
    pool.join()
    return errors

def fmean_squared_error(ground_truth, predictions):
    """
        Returns the root mean squared error between the predicted and actual labels of the test data

        Arguments:
        - predictions - the predicted labels of the test data
        - ground_truth - the actual labels of the test data

        Returns:
        - root mean squared error between y_pred and y_truth
    """
    
    fmean_squared_error_ = mean_squared_error(ground_truth, predictions)**0.5
    return fmean_squared_error_

#%% Features

"""
    Features
"""
      
class cust_w2v(BaseEstimator, TransformerMixin):
    def fit(self, x, y=None):
        return self
    def transform(self, features):
        dropcols=['id','search_term','product_title','product_description','brand','product_uid']
        features = features.drop(dropcols,axis=1)[['norm_sum_max','max_sim','norm_sum_sd','mean','mean_90','sim1gram','close1gram']].values
        return features
    
class cust_w2v_new(BaseEstimator, TransformerMixin):
    def fit(self, x, y=None):
        return self
    def transform(self, features):
        dropcols=['id','search_term','product_title','product_description','brand','product_uid']
        features = features.drop(dropcols,axis=1)[['norm_sum_max_own','max_sim_own','norm_sum_sd_own','mean_own','mean_90_own','sim1gram_own','close1gram_own']].values
        return features


class cust_grams(BaseEstimator, TransformerMixin):
    def fit(self, x, y=None):
        return self
    def transform(self, features):
        dropcols=['id','search_term','product_title','product_description','brand','product_uid']
        features = features.drop(dropcols,axis=1)[['attr1gram','attr2gram','attr3gram','descr1gram','descr2gram','descr3gram','title1gram','title2gram','title3gram','descr_allgram', 'title_allgram']].values
        return features
        
class cust_ratio(BaseEstimator, TransformerMixin):
    def fit(self, x, y=None):
        return self
    def transform(self, features):
        dropcols=['id','search_term','product_title','product_description','brand','product_uid']
        features = features.drop(dropcols,axis=1)[['attr1gram_ratio','attr2gram_ratio','attr3gram_ratio','descr1gram_ratio','descr2gram_ratio','descr3gram_ratio','title1gram_ratio','title2gram_ratio','title3gram_ratio']].values
        return features

class cust_rel(BaseEstimator, TransformerMixin):
    def fit(self, x, y=None):
        return self
    def transform(self, features):
        dropcols=['id','search_term','product_title','product_description','brand','product_uid']
        features = features.drop(dropcols,axis=1)[['rel_ind_desc','rel_ind_title','rel_attr1gram','rel_attr2gram','rel_attr3gram','rel_descr1gram','rel_descr2gram','rel_descr3gram','rel_title1gram','rel_title2gram','rel_title3gram','rel_ind_dis_title']].values
        return features
        
class cust_basics(BaseEstimator, TransformerMixin):
    def fit(self, x, y=None):
        return self
    def transform(self, features):
        dropcols=['id','search_term','product_title','product_description','brand','product_uid']
        features = features.drop(dropcols,axis=1)[['has_attributes','query_length','has_brandname', 'brandname_present','desc_length']].values
        return features
        
class cust_comp(BaseEstimator, TransformerMixin):
    def fit(self, x, y=None):
        return self
    def transform(self, features):
        dropcols=['id','search_term','product_title','product_description','brand','product_uid']
        features = features.drop(dropcols,axis=1)[['attr_abbrgram','title_abbrgram','descr_abbrgram','comp_gram_part','comp_gram','title_comp_gram','title_comp_gram_part','descr_comp_gram','descr_comp_gram_part',]].values
        return features
#    Train_Basics = X_train[['id','has_attributes','query_length','has_brandname', 'brandname_present','desc_length']]
#    Test_Basics = test[['id','has_attributes','query_length','has_brandname', 'brandname_present','desc_length']]
#    Test_Abbr_Comp = test[['id','attr_abbrgram','title_abbrgram','descr_abbrgram','comp_gram_part','comp_gram','title_comp_gram','title_comp_gram_part','descr_comp_gram','descr_comp_gram_part',]]
#    Train_Abbr_Comp = X_train[['id','attr_abbrgram','title_abbrgram','descr_abbrgram','comp_gram_part','comp_gram','title_comp_gram','title_comp_gram_part','descr_comp_gram','descr_comp_gram_part','descr_allgram', 'title_allgram']]
#    

class cust_all(BaseEstimator, TransformerMixin):
    def fit(self, x, y=None):
        return self
    def transform(self, features):
        dropcols=['id','search_term','product_title','product_description','brand','product_uid']
        features = features.drop(dropcols,axis=1).values
        return features

class cust_tfidf_title(BaseEstimator, TransformerMixin):
    def fit(self, x, y=None):
        return self
    def transform(self, features):
        tfidf2 = TfidfVectorizer(ngram_range=(1, 1), stop_words='english')
        tsvd2 = TruncatedSVD(n_components=10, random_state = 2016)
        tfidf2.fit(cust_txt_col(key='product_title').transform(features))
        res = tfidf2.transform(cust_txt_col(key='search_term').transform(features))
        tsvd2.fit(res)
        res = tsvd2.transform(res)
        return res

class cust_tfidf_descr(BaseEstimator, TransformerMixin):
    def fit(self, x, y=None):
        return self
    def transform(self, features):
        tfidf2 = TfidfVectorizer(ngram_range=(1, 1), stop_words='english')
        tsvd2 = TruncatedSVD(n_components=10, random_state = 2016)
        tfidf2.fit(cust_txt_col(key='product_description').transform(features))
        res = tfidf2.transform(cust_txt_col(key='search_term').transform(features))
        tsvd2.fit(res)
        res = tsvd2.transform(res)
        return res   
    
class cust_txt_col(BaseEstimator, TransformerMixin):
    def __init__(self, key):
        self.key = key
    def fit(self, x, y=None):
        return self
    def transform(self, data_dict):
        return data_dict[self.key].apply(str)

#%% Main classification pipeline

"""
    Main classification pipeline
"""      

if __name__  == '__main__':
    RMSE = make_scorer(fmean_squared_error, greater_is_better=False)
    (X_train, Y_train, test, y_all,train_add,test_add) = load_files()
    headers=['id','has_attributes','query_length','has_brandname','attr1gram','attr2gram','attr3gram','descr1gram','descr2gram',
    'descr3gram','title1gram','title2gram','title3gram','attr1gram_ratio','attr2gram_ratio','attr3gram_ratio','descr1gram_ratio','descr2gram_ratio','descr3gram_ratio','title1gram_ratio','title2gram_ratio','title3gram_ratio','brandname_present','rel_ind_desc','rel_ind_title','rel_attr1gram','rel_attr2gram','rel_attr3gram','rel_descr1gram','rel_descr2gram',
    'rel_descr3gram','rel_title1gram','rel_title2gram','rel_title3gram','norm_sum_max', 'max_sim', 'norm_sum_sd', 'mean', 'mean_90','attr_abbrgram','title_abbrgram','descr_abbrgram','comp_gram_part','comp_gram','title_comp_gram','title_comp_gram_part','descr_comp_gram','descr_comp_gram_part','descr_allgram', 'title_allgram','desc_length','norm_sum_max_own', 'max_sim_own', 'norm_sum_sd_own', 'mean_own', 'mean_90_own','sim1gram','sim1gram_own','close1gram', 'close1gram_own','rel_ind_dis_title','d2v_google', 'd2v_own', 'd2v_title_google','d2v_title_own', 'norm_sum_max_title', 'max_sim_title', 'norm_sum_sd_title', 'mean_title', 'mean_90_title', 'norm_sum_max_own_title', 'max_sim_own_title', 'norm_sum_sd_own_title', 'mean_own_title', 'mean_90_own_title', 'sim1gram_title', 'sim1gram_own_title', 'close1gram_title', 'close1gram_own_title']
    X_train = pd.DataFrame(data=X_train, columns=headers)
    test = pd.DataFrame(data=test, columns=headers)
    X_train = pd.merge(X_train, train_add, how='left', on='id').drop(['relevance'],axis=1)
    test = pd.merge(test, test_add, how='left', on='id')
    X_train_known = X_train[7500:]
    Y_train_known = Y_train[7500:]
    X_train_unknown = X_train[:7500]
    Y_train_unknown = Y_train[:7500]
    #%%

##    #%%
#    starttime = time.time()
#    print('Started training @ '+str(datetime.datetime.now()))
#    sys.stdout.flush()
#    folds = 8
#    #ADA = cross_val(X_train, Y_train, AdaBoost, n_folds=folds, randomized=True)
#    params = {}
#    #params['forest_n_estimators'] = [500]
#    #params['forest_max_depth'] = [20]
#    rf = RandomForestRegressor(n_estimators=500, max_depth=20,max_features=20)
#    #clf = BaggingRegressor(rf, n_estimators=bagging_n_estimators, max_samples=max_samples)
#    #ypred = ApplyClassifier(RandomForest,params, X_train, Y_train, X_train_test,Y_test=None, make_submission=False)
#    model = grid_search.GridSearchCV(estimator = rf, param_grid = params, n_jobs = -1, cv = 2, verbose = 20, scoring=RMSE)
#    model.fit(cust_all().transform(X_train), Y_train)
##    
#    
    #%%    
#    param = {}
#    param['booster'] = 'gbtree'
#    param['objective'] = 'binary:logistic'
#    param["eval_metric"] = "error"
#    param['eta'] = 0.3
#    param['gamma'] = 0
#    param['max_depth'] = 6
#    param['min_child_weight']=1
#    param['max_delta_step'] = 0
#    param['subsample']= 1
#    param['colsample_bytree']=1
#    param['silent'] = 1
#    param['seed'] = 0
#    param['base_score'] = 0.5
#    param['n_estimators'] = 100
#    param['learning_rate=0.05'] = 0.0.5
#    param = {}
#    param['booster'] = 'gblinear'
#    param['objective'] = 'reg:linear'
#    param["eval_metric"] = "MSE"
#    param['eta'] = 0.3
#    param['gamma'] = 0
#    param['max_depth'] = 6
#    param['min_child_weight']=1
#    param['max_delta_step'] = 0
#    param['subsample']= 1
#    param['colsample_bytree']=1
#    param['silent'] = 1
#    param['seed'] = 0
#    param['base_score'] = 0.5
#    param['n_estimators'] = 100
#    param['learning_rate'] = 0.05
    rfr = xgb.XGBRegressor(objective = "reg:linear",max_depth = 8, gamma = 0 , n_estimators = 500, learning_rate = 0.05)
    #rfr = xgb.train(param,)
    #rfr = RandomForestRegressor(n_estimators = 4000, n_jobs = -1, random_state = 2016, verbose = 1, max_depth = 28, max_features = 20,)
    #rfr = AdaBoostRegressor(n_estimators = 50, random_state = 2016)# max_depth = 20, max_features = 20)

    #bag = BaggingRegressor(rfr, n_estimators=20, max_samples=0.15)
    tfidf = TfidfVectorizer(ngram_range=(1, 1), stop_words='english')
    tsvd = TruncatedSVD(n_components=10, random_state = 2016)

    clf = pipeline.Pipeline([
            ('union', FeatureUnion(
                        transformer_list = [
#                            ('cst2', pipeline.Pipeline([('cust2',cust2()),('rfr2', rfr)])),
#                            ('cst3', pipeline.Pipeline([('cust3',cust3()),('rfr3', rfr)])),cust_all, cust_w2v, cust_grams, cust_ratio, cust_rel
                            ('cst2', cust_w2v()),
                            ('cst11', cust_w2v_new()),
                            ('cst3', cust_ratio()),
                            ('cst10', cust_comp()),
                            ('cst4', cust_rel()),
                            ('cst5', cust_grams()),
                            ('cst6', cust_all()),
                            ('cst7', cust_basics()),#,
                            #('cst8', cust_tfidf_title()),
                            #('cst9', cust_tfidf_descr()),
                            ('txt1', pipeline.Pipeline([('s1', cust_txt_col(key='search_term')), ('tfidf1', tfidf), ('tsvd1', tsvd)])),
                            ('txt2', pipeline.Pipeline([('s2', cust_txt_col(key='product_title')), ('tfidf2', tfidf), ('tsvd2', tsvd)])),
                            ('txt3', pipeline.Pipeline([('s3', cust_txt_col(key='product_description')), ('tfidf3', tfidf), ('tsvd3', tsvd)])),
                            ('txt4', pipeline.Pipeline([('s4', cust_txt_col(key='brand')), ('tfidf4', tfidf), ('tsvd4', tsvd)]))
                            ],
# BEST WEIGHTS SO FAR                            
                        transformer_weights = {
                            'cst2': 0.75,
                            'cst3': 0.5,
                            'cst10': 0.2,
                            'cst4': 0.5,
                            'cst5': 0.5,
                            'cst6': 0.75,
                            'cst7': 0.5,
                            #'cst8': 0.0,
                            'cst11': 0.5,
                            'txt1': 0.6,
                            'txt2': 0.5,
                            'txt3': 0.0,
                            'txt4': 0.6
                            },
#                        transformer_weights = {
#                            'cst2': 1.0,
#                            'cst3': 1.0,
#                            'cst10': 0.0,
#                            'cst11': 0.5,
#                            'cst4': 0.0,
#                            'cst5': 0.0,
#                            'cst6': 0.0,
#                            'cst7': 0.0#,
#                            #'cst8': 0.0,
#                            #'cst9': 0.0,
#                            #'txt1': 0.0,
#                            #'txt2': 0.0,
#                            #'txt3': 0.0,
#                            #'txt4': 0.0
#                            },
                    #n_jobs = -1
                    )), 
            ('rfr', rfr)])

    param = {}
    #clf.fit(X_train,Y_train)
    model = grid_search.GridSearchCV(estimator = clf, param_grid = param, n_jobs = -1, cv =2, verbose = 20, scoring=RMSE)
    model.fit(X_train_known, Y_train_known)
    #clf.fit(X_train, Y_train)
    
    
    
    print("Best parameters found by grid search:")
    print(model.best_params_)
    print("Best CV score:")
    print(model.best_score_)
    print(model.best_score_ + 0.47003199274)

    #y_pred = clf.predict(test)
    
    y_pred = model.predict(test)
    #print(RMSE2(y_pred,Y_train_unknown))
    pd.DataFrame({"id": [int(x) for x in test['id']], "relevance": y_pred}).to_csv('submission.csv',index=False)    
    

    #%%
#    headers=['id','has_attributes','query_length','has_brandname','attr1gram','attr2gram','attr3gram','descr1gram','descr2gram',
#    'descr3gram','title1gram','title2gram','title3gram','attr1gram_ratio','attr2gram_ratio','attr3gram_ratio','descr1gram_ratio','descr2gram_ratio','descr3gram_ratio','title1gram_ratio','title2gram_ratio','title3gram_ratio','brandname_present','rel_ind_desc','rel_ind_title','rel_attr1gram','rel_attr2gram','rel_attr3gram','rel_descr1gram','rel_descr2gram',
#    'rel_descr3gram','rel_title1gram','rel_title2gram','rel_title3gram','norm_sum_max', 'max_sim', 'norm_sum_sd', 'mean', 'mean_90','attr_abbrgram','title_abbrgram','descr_abbrgram','comp_gram_part','comp_gram','title_comp_gram','title_comp_gram_part','descr_comp_gram','descr_comp_gram_part','descr_allgram', 'title_allgram','desc_length']
#    X_train_all = X_train
#    test_all = test
#    Y_train_all = Y_train
#    Y_train_test = Y_train[65000:]
#    Y_train = Y_train[:65000]
#    X_train = pd.DataFrame(data=X_train, columns=headers)[:65000]
#    test = pd.DataFrame(data=test, columns=headers)[:65000]
#    Train_Basics = X_train[['id','has_attributes','query_length','has_brandname', 'brandname_present','desc_length']]
#    Test_Basics = test[['id','has_attributes','query_length','has_brandname', 'brandname_present','desc_length']]
#    Train_Grams = X_train[['id','attr1gram','attr2gram','attr3gram','descr1gram','descr2gram','descr3gram','title1gram','title2gram','title3gram']]
#    Test_Grams = test[['id','attr1gram','attr2gram','attr3gram','descr1gram','descr2gram','descr3gram','title1gram','title2gram','title3gram']]
#    Train_Grams_Ratio = X_train[['id','attr1gram_ratio','attr2gram_ratio','attr3gram_ratio','descr1gram_ratio','descr2gram_ratio','descr3gram_ratio','title1gram_ratio','title2gram_ratio','title3gram_ratio']]
#    Test_Grams_Ratio = test[['id','attr1gram_ratio','attr2gram_ratio','attr3gram_ratio','descr1gram_ratio','descr2gram_ratio','descr3gram_ratio','title1gram_ratio','title2gram_ratio','title3gram_ratio']]
#    Test_RelGrams = test[['id','rel_ind_desc','rel_ind_title','rel_attr1gram','rel_attr2gram','rel_attr3gram','rel_descr1gram','rel_descr2gram','rel_descr3gram','rel_title1gram','rel_title2gram','rel_title3gram']]
#    Train_RelGrams = X_train[['id','rel_ind_desc','rel_ind_title','rel_attr1gram','rel_attr2gram','rel_attr3gram','rel_descr1gram','rel_descr2gram','rel_descr3gram','rel_title1gram','rel_title2gram','rel_title3gram']]    
#    Test_Word2Vec = test[['id','norm_sum_max','max_sim','norm_sum_sd','mean','mean_90']]
#    Train_Word2Vec = X_train[['id','norm_sum_max','max_sim','norm_sum_sd','mean','mean_90']]
#    Test_Abbr_Comp = test[['id','attr_abbrgram','title_abbrgram','descr_abbrgram','comp_gram_part','comp_gram','title_comp_gram','title_comp_gram_part','descr_comp_gram','descr_comp_gram_part','descr_allgram', 'title_allgram']]
#    Train_Abbr_Comp = X_train[['id','attr_abbrgram','title_abbrgram','descr_abbrgram','comp_gram_part','comp_gram','title_comp_gram','title_comp_gram_part','descr_comp_gram','descr_comp_gram_part','descr_allgram', 'title_allgram']]
#    
#    X_train = np.matrix(X_train)    
#    test = np.matrix(test)
#    Train_Basics = np.matrix(Train_Basics)    
#    Test_Basics = np.matrix(Test_Basics)
#    Train_Grams = np.matrix(Train_Grams)    
#    Test_Grams = np.matrix(Test_Grams)
#    Train_Grams_Ratio = np.matrix(Train_Grams_Ratio)    
#    Test_Grams_Ratio = np.matrix(Test_Grams_Ratio)
#    Train_RelGrams = np.matrix(Train_RelGrams)    
#    Test_RelGrams = np.matrix(Test_RelGrams)
#    Train_Word2Vec = np.matrix(Train_Word2Vec)    
#    Test_Word2Vec = np.matrix(Test_Word2Vec)
#    Train_Abbr_Comp = np.matrix(Train_Abbr_Comp)    
#    Test_Abbr_Comp = np.matrix(Test_Abbr_Comp)
#    
#    X_train = pd.DataFrame(data=X_train_all, columns=headers)[65000:]
#    test = pd.DataFrame(data=test_all, columns=headers)[65000:]
#    Train_Basics_test = X_train[['id','has_attributes','query_length','has_brandname', 'brandname_present','desc_length']]
#    Test_Basics_test = test[['id','has_attributes','query_length','has_brandname', 'brandname_present','desc_length']]
#    Train_Grams_test = X_train[['id','attr1gram','attr2gram','attr3gram','descr1gram','descr2gram','descr3gram','title1gram','title2gram','title3gram']]
#    Test_Grams_test = test[['id','attr1gram','attr2gram','attr3gram','descr1gram','descr2gram','descr3gram','title1gram','title2gram','title3gram']]
#    Train_Grams_Ratio_test = X_train[['id','attr1gram_ratio','attr2gram_ratio','attr3gram_ratio','descr1gram_ratio','descr2gram_ratio','descr3gram_ratio','title1gram_ratio','title2gram_ratio','title3gram_ratio']]
#    Test_Grams_Ratio_test = test[['id','attr1gram_ratio','attr2gram_ratio','attr3gram_ratio','descr1gram_ratio','descr2gram_ratio','descr3gram_ratio','title1gram_ratio','title2gram_ratio','title3gram_ratio']]
#    Test_RelGrams_test = test[['id','rel_ind_desc','rel_ind_title','rel_attr1gram','rel_attr2gram','rel_attr3gram','rel_descr1gram','rel_descr2gram','rel_descr3gram','rel_title1gram','rel_title2gram','rel_title3gram']]
#    Train_RelGrams_test = X_train[['id','rel_ind_desc','rel_ind_title','rel_attr1gram','rel_attr2gram','rel_attr3gram','rel_descr1gram','rel_descr2gram','rel_descr3gram','rel_title1gram','rel_title2gram','rel_title3gram']]    
#    Test_Word2Vec_test = test[['id','norm_sum_max','max_sim','norm_sum_sd','mean','mean_90']]
#    Train_Word2Vec_test = X_train[['id','norm_sum_max','max_sim','norm_sum_sd','mean','mean_90']]
#    Test_Abbr_Comp_test = test[['id','attr_abbrgram','title_abbrgram','descr_abbrgram','comp_gram_part','comp_gram','title_comp_gram','title_comp_gram_part','descr_comp_gram','descr_comp_gram_part','descr_allgram', 'title_allgram']]
#    Train_Abbr_Comp_test = X_train[['id','attr_abbrgram','title_abbrgram','descr_abbrgram','comp_gram_part','comp_gram','title_comp_gram','title_comp_gram_part','descr_comp_gram','descr_comp_gram_part','descr_allgram', 'title_allgram']]
# 
#    X_train_test = np.matrix(pd.DataFrame(data=X_train_all, columns=headers)[65000:])    
#    X_train = np.matrix(pd.DataFrame(data=X_train_all, columns=headers)[:65000])  
#    test = np.matrix(test)
#    Train_Basics_test = np.matrix(Train_Basics_test)    
#    Test_Basics_test = np.matrix(Test_Basics_test)
#    Train_Grams_test = np.matrix(Train_Grams_test)    
#    Test_Grams_test = np.matrix(Test_Grams_test)
#    Train_RelGrams_test = np.matrix(Train_RelGrams_test)
#    Test_RelGrams_test = np.matrix(Test_RelGrams_test)
#    Train_Grams_Ratio_test = np.matrix(Train_Grams_Ratio_test)    
#    Test_Grams_Ratio_test = np.matrix(Test_Grams_Ratio_test)
#    Train_Word2Vec_test = np.matrix(Train_Word2Vec_test)    
#    Test_Word2Vec_test = np.matrix(Test_Word2Vec_test)
#    Train_Abbr_Comp_test = np.matrix(Train_Abbr_Comp_test)    
#    Test_Abbr_Comp_test = np.matrix(Test_Abbr_Comp_test)
#    
#    
#    #%%
#    
#    starttime = time.time()
#    print('Started training @ '+str(datetime.datetime.now()))
#    sys.stdout.flush()
#    #folds = 8
#    #ADA = cross_val(X_train, Y_train, AdaBoost, n_folds=folds, randomized=True)
#    params = {}
#    params['forest_n_estimators'] = 50
#    params['forest_max_depth'] = 10
#    params['bagging_n_estimators'] = 20
#    params2 = {}
#    params2['forest_n_estimators'] = 50
#    params2['forest_max_depth'] = 10
#    params2['bagging_n_estimators'] = 20
#    
#    #rf = RandomForestRegressor(n_estimators=forest_n_estimators, max_depth=forest_max_depth)
#    #clf = BaggingRegressor(rf, n_estimators=bagging_n_estimators, max_samples=max_samples)
#    y_pred_train = ApplyClassifier(RandomForest,params, Train_Basics, Y_train, Train_Basics)
#    print('done Basics')
#    print(RMSE(y_pred_train,Y_train))
#    y_pred2_train = ApplyClassifier(RandomForest,params, Train_RelGrams, Y_train, Train_RelGrams)
#    print('done RelGrams')
#    print(RMSE(y_pred2_train,Y_train))
#    y_pred3_train = ApplyClassifier(RandomForest,params2, Train_Grams, Y_train, Train_Grams, Y_test=None, make_submission=False)
#    print('done Grams')
#    print(RMSE(y_pred3_train,Y_train))
#    y_pred7_train = ApplyClassifier(RandomForest,params2, Train_Grams_Ratio, Y_train, Train_Grams_Ratio, Y_test=None, make_submission=False)
#    print('done Grams ratio')
#    print(RMSE(y_pred7_train,Y_train))
#    y_pred4_train = ApplyClassifier(RandomForest,params, Train_Word2Vec, Y_train, Train_Word2Vec, Y_test=None, make_submission=False)
#    print('done Word2Vec')
#    print(RMSE(y_pred4_train,Y_train))
#    y_pred5_train = ApplyClassifier(RandomForest,params, Train_Abbr_Comp, Y_train, Train_Abbr_Comp,Y_test=None, make_submission=False)
#    print('done Abbr_Comp')
#    print(RMSE(y_pred5_train,Y_train))
#    y_pred6_train = ApplyClassifier(RandomForest,params, X_train, Y_train, X_train,Y_test=None, make_submission=False)
#    print('done Combined')
#    print(RMSE(y_pred6_train,Y_train))
#    id_test = X_train[:, 0].astype(np.int)
#    id_test = id_test.reshape(65000)
#    id_test = id_test.tolist()
#    id_test = np.array(id_test[0])
#    y_all_train = np.matrix([id_test,y_pred_train,y_pred2_train,y_pred3_train,y_pred4_train,y_pred5_train,y_pred6_train,y_pred7_train])
#    y_all_train = y_all_train.transpose()
#
#    #%%
#    
#    starttime = time.time()
#    print('Started training @ '+str(datetime.datetime.now()))
#    sys.stdout.flush()
#    #folds = 8
#    #ADA = cross_val(X_train, Y_train, AdaBoost, n_folds=folds, randomized=True)
#    params = {}
#    params['forest_n_estimators'] = 50
#    params['forest_max_depth'] = 10
#    params['bagging_n_estimators'] = 20
#    #rf = RandomForestRegressor(n_estimators=forest_n_estimators, max_depth=forest_max_depth)
#    #clf = BaggingRegressor(rf, n_estimators=bagging_n_estimators, max_samples=max_samples)
#    y_pred_test = ApplyClassifier(RandomForest,params, Train_Basics, Y_train, Train_Basics_test)
#    print('done Basics')
#    print(RMSE(y_pred_test,Y_train_test))
#    y_pred2_test = ApplyClassifier(RandomForest,params, Train_RelGrams, Y_train, Train_RelGrams_test)
#    print('done RelGrams')
#    print(RMSE(y_pred2_test,Y_train_test))
#    y_pred3_test = ApplyClassifier(RandomForest,params2, Train_Grams, Y_train, Train_Grams_test, Y_test=None, make_submission=False)
#    print('done Grams')
#    print(RMSE(y_pred3_test,Y_train_test))
#    y_pred7_test = ApplyClassifier(RandomForest,params2, Train_Grams_Ratio, Y_train, Train_Grams_Ratio_test, Y_test=None, make_submission=False)
#    print('done Grams')
#    print(RMSE(y_pred7_test,Y_train_test))
#    y_pred4_test = ApplyClassifier(RandomForest,params, Train_Word2Vec, Y_train, Train_Word2Vec_test, Y_test=None, make_submission=False)
#    print('done Word2Vec')
#    print(RMSE(y_pred4_test,Y_train_test))
#    y_pred5_test = ApplyClassifier(RandomForest,params, Train_Abbr_Comp, Y_train, Train_Abbr_Comp_test,Y_test=None, make_submission=False)
#    print('done Abbr_Comp')
#    print(RMSE(y_pred5_test,Y_train_test))
#    y_pred6_test = ApplyClassifier(RandomForest,params, X_train, Y_train, X_train_test,Y_test=None, make_submission=False)
#    print('done Combined')
#    print(RMSE(y_pred6_test,Y_train_test))
#    id_test2 = X_train_test[:, 0].astype(np.int)
#    id_test2 = id_test2.reshape(9067)
#    id_test2 = id_test2.tolist()
#    id_test2 = np.array(id_test2[0])
#    y_all_train2 = np.matrix([id_test2,y_pred_test,y_pred2_test,y_pred3_test,y_pred4_test,y_pred5_test,y_pred6_test,y_pred7_test])
#    y_all_train2 = y_all_train2.transpose()
#    y_all_pred = ApplyClassifier(RandomForest,params, y_all_train, Y_train, y_all_train2,Y_test=None, make_submission=False)
#    RMSE(y_all_pred,Y_train_test)
#    #%%
#    y_pred = ApplyClassifier(RandomForest,params, Train_Basics, Y_train, Test_Basics)
#    print('done Basics pred')
#    #print(RMSE(y_pred,Y_train))
#    y_pred2 = ApplyClassifier(RandomForest,params, Train_RelGrams, Y_train, Test_RelGrams)
#    print('done RelGrams pred')
#    #print(RMSE(y_pred2,Y_train))
#    y_pred3 = ApplyClassifier(RandomForest,params, Train_Grams, Y_train, Test_Grams, Y_test=None, make_submission=False)
#    print('done Grams pred')
#    #print(RMSE(y_pred3,Y_train))
#    y_pred4 = ApplyClassifier(RandomForest,params, Train_Word2Vec, Y_train, Test_Word2Vec, Y_test=None, make_submission=False)
#    print('done Word2Vec pred')
#    #print(RMSE(y_pred4,Y_train))
#    y_pred5 = ApplyClassifier(RandomForest,params, Train_Abbr_Comp, Y_train, Test_Abbr_Comp,Y_test=None, make_submission=False)
#    print('done Abbr_Comp pred')
#    #print(RMSE(y_pred5,Y_train))
#    y_pred6 = ApplyClassifier(RandomForest,params, X_train, Y_train, test,Y_test=None, make_submission=False)
#    print('done Combined pred')
#    #print(RMSE(y_pred5,Y_train))
#    id_test2 = test[:, 0].astype(np.int)
#    id_test2 = id_test2.reshape(166693)
#    id_test2 = id_test2.tolist()
#    id_test2 = np.array(id_test2[0])
#    y_all = np.matrix([id_test2,y_pred,y_pred2,y_pred3,y_pred4,y_pred5,y_pred6])
#    y_all = y_all.transpose()
#    y_all_pred = ApplyClassifier(RandomForest,params, y_all_train, Y_train, y_all,Y_test=None, make_submission=False)
#    print('done All pred')#    starttime = time.time()
#    print('Started training @ '+str(datetime.datetime.now()))
#    sys.stdout.flush()
#    folds = 8
#    #ADA = cross_val(X_train, Y_train, AdaBoost, n_folds=folds, randomized=True)
#    #RF = cross_val(X_train, Y_train, RandomForest, n_folds=folds, randomized=True)
##    CR = cross_val(X_train, Y_train, ClassifierRidge, n_folds=folds, randomized=True)
#    
#    clf = BaggingRegressor(RandomForestRegressor(n_estimators = 400, max_depth = 10))
#    param_grid = {'n_estimators': [50,100]}
#    model = GridSearchCV(estimator = clf, param_grid = param_grid, n_jobs = 8, cv = 2, verbose = 20, scoring=RMSE2)
#    model.fit(X_train, Y_train)
#    
#    print("Best parameters found by grid search:")
#    print(model.best_params_)
#    print("Best CV score:")
#    print(model.best_score_)
#    print(model.best_score_ + 0.47003199274)
#    #    
#    print('Training time: {0} sec.'.format(formatTime(time.time() - starttime)))    
    
    #fig, axes = plt.subplots()
    #axes.set_ylim([np.min((ADA,RF))-0.01,np.max((ADA,RF))+0.01])
    #axes.set_title(str(folds)+" fold crossvalidation")
    #axes.boxplot([ADA,RF], labels=['AdaBoost', 'Random forest'], showmeans=True)
    #plt.show()
    
#    y_pred = RandomForest(X_train[:35000],Y_train[:35000], X_train[35000:])
