# README #

This README would normally document whatever steps are necessary to get your application up and running.

### How do I get set up? ###
You can use the following code to import the dataset and start working with it.

```
#!python
import pickle

train_file = open('thebigdata/dataset/train_set.pkl', 'rb')
train = pickle.load(train_file) #this is a list of dictionaries
products_file = open('thebigdata/dataset/products.pkl', 'rb')
products = pickle.load(products_file) #this is a dictionary with as keys the product uid and as value a dictionary containing a dictionary of attributes and a product description

#example data retrieval
print(train[0]) #prints the dictionary of the first entry
print(train[0]['product_uid']) #prints the product uid of the first entry
print(products[train[0]['product_uid']]) #prints all info on the first product in the train set
print(products[train[0]['product_uid']]['product_description']) #prints the product description of the first product in the train set, note the nested indexing
print(products[train[0]['product_uid']]['attributes'].popitem()) #print the first attribute-value pair of this product

#for stemming we can use
from nltk.stem import WordNetLemmatizer
wnl = WordNetLemmatizer()
wnl.lemmatize('am','v') #for verbs

```
Conceptual database: http://www.nltk.org/api/nltk.corpus.reader.html#module-nltk.corpus.reader.wordnet

Preprocessing the data can be done via *pipeline.py* and please note that *pipeline_par.py* is currently outdated and due to parallelized code objects are not accessed by reference which means the code currently **does not** work.

After preprocessing, the train set consists of a list of dictionary with entries 'id', 'product_uid', 'product_title', 'search_term' and 'relevance'. Note that both the search terms and products titles have been preprocessed and now consist of a list of dictionaries each containing 'stem', 'original', 'wordtype', 'simplified_wordtype', 'index', 'relative_index'. The test set contains the same fields only without the relevance field. The products set is a dictionary with as keys 'product_uid' and as values a dictionary containing the keys 'attributes' and 'product_description'. 'attributes' itself is again a dictionary with as keys the attribute names and as values the attribute values. Note that attribute keys, values and 'product_description' are all processed into a list of dictionaries with again the fields 'stem', 'original', 'wordtype', 'simplified_wordtype', 'index', 'relative_index' just like before.