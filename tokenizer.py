import nltk

PUNCTUATION = (';', ':', ',', '.', '!', '?', '(', ')', '/')


def create_tokens(text):
    tokens_init = nltk.word_tokenize(text)
    tokens = []
    for token in tokens_init:
        if token not in PUNCTUATION:
            tokens.append(token)
    return tokens
