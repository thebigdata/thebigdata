### Returns for each word in search term the stats [mean, maximum similarity, amount of words with similarity above 1 sd, mean of 90%tile, standard deviation]

from gensim.models import Word2Vec as w2v
import statistics
from scipy import spatial as sp

class Word2Vec:
    def __init__(self):
        self.model = w2v.load_word2vec_format("./dataset/GoogleNews-vectors-negative300.bin", binary=True)
        self.model2 = w2v.load("./dataset/own_w2v_model")
        

#%%
    def get_features(self, search_term, prod_term, word_types):
        stats = self.get_w2v_stats(search_term, prod_term, word_types)
        stats_own = []
        stats_google = []
        if stats:
            if stats[0]:
                norm_sum_max = sum(x['max'] for x in stats[0])/len(stats[0])
                max_sim = max(x['max'] for x in stats[0])
                norm_sum_sd = sum(x['std'] for x in stats[0])/len(stats[0])
                mean = sum(x['mean'] for x in stats[0])/len(stats[0])
                mean_90 = sum(x['mean_90th_percentile'] for x in stats[0])/len(stats[0])
                sim1gram = sum(x['sim1gram'] for x in stats[0])
                close1gram = sum(x['close1gram'] for x in stats[1])
                stats_google =[norm_sum_max, max_sim, norm_sum_sd, mean, mean_90,sim1gram,close1gram]
            if stats[1]:
                norm_sum_max = sum(x['max'] for x in stats[1])/len(stats[1])
                max_sim = max(x['max'] for x in stats[1])
                norm_sum_sd = sum(x['std'] for x in stats[1])/len(stats[1])
                mean = sum(x['mean'] for x in stats[1])/len(stats[1])
                mean_90 = sum(x['mean_90th_percentile'] for x in stats[1])/len(stats[1])
                sim1gram = sum(x['sim1gram'] for x in stats[1])
                close1gram = sum(x['close1gram'] for x in stats[1])
                stats_own =[norm_sum_max, max_sim, norm_sum_sd, mean, mean_90,sim1gram,close1gram]
        return stats_google, stats_own
        
        
    def d2v (self, st, pd):
        # init vecs
        pdvec = 0
        stvec = 0
        pdvec2 = 0
        stvec2 = 0
        # remove duplicates
        vocab = set([])
        oldvl = len(vocab)
        dist = 0
        dist2 = 0
        # first pd
        for word in pd:
            #		if word[1] == 'N':
            w = word[0]
            if len(w) > 1:
                vocab.add(w)
                newvl = len(vocab)
                if newvl > oldvl:
                    try:
                        pdvec = pdvec + self.model[w]
                        pdvec2 = pdvec2 + self.model2[w]
                    except:
                        #print(" missing")
                        #print("Not in vocab: "+search_word[0]+" and "+prod_word[0])
                        pass
                oldvl = newvl
        # now the st			
        for word in st:
            w = word[0]
            try:
                stvec = stvec + self.model[w]
                stvec2 = stvec2 + self.model2[w]
                #print(w)
            except:
                #print("Not in vocab: "+search_word[0]+" and "+prod_word[0])
                pass
            # save to file
            #	pdvec = pdvec/len(pd)
            #	stvec = stvec/len(st)
        try:
            # average
            dist = sp.distance.cosine(pdvec, stvec)
            dist2 = sp.distance.cosine(pdvec2, stvec2)
            #		print(dist)
        except:
            pass
        return (dist,dist2)

        
#%%
    # what about duplicate words??
    def get_w2v_stats(self,search_term, prod_term, word_types):
        stats_all_own = []
        stats_all_google = []
        stats_all = []
        for search_word in search_term:
            most_similar = []
            try:
                most_similar = [self.model2.most_similar(search_word[0])]
            except:
                pass
            stats = dict.fromkeys(['mean','max','prop_above_std','mean_90th_percentile','std','sim1gram','close1gram'])
            stats['sim1gram']=0
            stats['close1gram']=0
            sims = []
            word_posTag = search_word[1]
            if word_posTag in word_types:
                for prod_word in prod_term:
                    posTag = prod_word[1]
                    if posTag == word_posTag:
                        try:
                            s = self.model.similarity(search_word[0], prod_word[0])
                            if s < 0.98: 
                                sims.append(s)
                            if s> 0.6:
                                stats['sim1gram'] += 1
                            if prod_word[0] in [x[0] for x in most_similar]:
                                stats['close1gram'] += 1
                        except:
                            #print("Not in vocab: "+search_word[0]+" and "+prod_word[0])
                            pass
                
                if sims:
                    sims.sort(reverse = True)
                    
                    sim_avg = sum(sims)/len(sims)
                    stats['mean'] = sim_avg				# mean
                    stats['max'] = max(sims)				# max
                    	
                    count = 0
                    sdev = statistics.pstdev(sims)
                    for val in sims:
                        if val > sim_avg+sdev:
                            count = count+1
                    
                    stats['prop_above_std'] = count/len(sims)		# proportion above 1 SD
                    
                    # mean of 90th percentile
                    stats['mean_90th_percentile'] = statistics.mean(sims[0:round(0.1*len(sims))+1])
                    stats['std'] = sdev
                else:
                    stats['mean'] = 0.0
                    stats['max'] = 0.0
                    stats['prop_above_std'] = 0.0
                    stats['mean_90th_percentile'] = 0.0
                    stats['std'] = 0.0
                stats_all_google.append(stats)
            else:
                stats['mean'] = 0.0
                stats['max'] = 0.0
                stats['prop_above_std'] = 0.0
                stats['mean_90th_percentile'] = 0.0
                stats['std'] = 0.0
                stats_all_google.append(stats)
                
        for search_word in search_term:
            most_similar = []
            try:
                most_similar = [self.model2.most_similar(search_word[0])]
            except:
                pass
            stats = dict.fromkeys(['mean','max','prop_above_std','mean_90th_percentile','std','sim1gram','close1gram'])
            sims = []
            stats['sim1gram']=0
            stats['close1gram']=0
            word_posTag = search_word[1]
            if word_posTag in word_types:
                for prod_word in prod_term:
                    posTag = prod_word[1]
                    if posTag == word_posTag:
                        try:
                            s = self.model2.similarity(search_word[0], prod_word[0])
                            if s < 0.98: 
                                sims.append(s)
                            if s> 0.5:
                                stats['sim1gram'] += 1
                            if prod_word[0] in [x[0] for x in most_similar]:
                                stats['close1gram'] += 1
                            
                        except:
                            #print("Not in vocab: "+search_word[0]+" and "+prod_word[0])
                            pass
                
                if sims:
                    sims.sort(reverse = True)
                    
                    sim_avg = sum(sims)/len(sims)
                    stats['mean'] = sim_avg				# mean
                    stats['max'] = max(sims)				# max
                    	
                    count = 0
                    sdev = statistics.pstdev(sims)
                    for val in sims:
                        if val > sim_avg+sdev:
                            count = count+1
                    
                    stats['prop_above_std'] = count/len(sims)		# proportion above 1 SD
                    
                    # mean of 90th percentile
                    stats['mean_90th_percentile'] = statistics.mean(sims[0:round(0.1*len(sims))+1])
                    stats['std'] = sdev
                else:
                    stats['mean'] = 0.0
                    stats['max'] = 0.0
                    stats['prop_above_std'] = 0.0
                    stats['mean_90th_percentile'] = 0.0
                    stats['std'] = 0.0
                stats_all_own.append(stats)
            else:
                stats['mean'] = 0.0
                stats['max'] = 0.0
                stats['prop_above_std'] = 0.0
                stats['mean_90th_percentile'] = 0.0
                stats['std'] = 0.0
                stats_all_own.append(stats)
        stats_all = [stats_all_google,stats_all_own]
        return stats_all

#TEST
#w2v = load_w2v_model()
#for i in range(0,10):
#    pos = ['J','N','V']
#    search = [["test",0,'N'], ["the",0,'J'], ["sink",0,'V']]
#    prod = [["test",0,'M'], ["the",0,'J'], ["sink",0,'V']]
#    
#    stats = get_w2v_stats(search, prod, pos, w2v)
#    print(stats)