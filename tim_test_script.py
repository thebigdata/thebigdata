# -*- coding: utf-8 -*-
"""
TimTestScript

Holds test cases for the functions in Accuracy, Distance and RandomForest

Tests the following functions:
Accuracy - accuracy, error
Distance - distance, minDistance, minDistanceList, minDistancePairedLists
RandomForest - trainRF, testRF

Created on Mon Feb 29 12:18:12 2016

@author: Tim
"""

#%% Imports

import numpy
from Accuracy import accuracy, error
from Distance import distance, minDistance, minDistanceList, minDistancePairedLists
from RandomForest import trainRF, testRF

#%% Test Cases  
    
'''

    TEST CASES

'''

if __name__ == '__main__':
    
    #%% Test Distance
    
    w1 = ('hallo', 'N', 1)
    w2 = ('halal', 'N', 1)
    
    dist = distance(w1, w2)
    print('distance:{0}'.format(dist))
    
    #%% Test min distance with multiple words
    
    wl = ['hallo', 'hello', 'helel']
    wl = [(w, 'N', 1) for w in wl]
    dist = minDistance(w1, wl)
    print('distance after minDistance:{0}'.format(dist))
    
    #%% Test min distance with multiple words lists
    
    dist = minDistanceList([w1,w2], wl)
    print('distance after minDistanceList:{0}'.format(dist))
    
    #%% Test Distance for multiple paired words lists
            
    search_terms = [['blue','blobs'],['doo', 'brown'],['hulu','subscription'],['holla','back','gurllll'],['halal'],['one','of','those','things','that','go','ding','dong']]
    descr = [['bla','blob','blue'],['herpy','derpy','doo'],['hallo','hulu','hodor'],['hello','holla','yubnub'],['hallo','halal'],['boom','shaka','laka','ding','dong']]
    search_terms = [[(w, 'N', 1) for w in d] for d in search_terms]
    descr = [[(w, 'N', 1) for w in d] for d in descr]
    print('\nsearch terms format: {0}'.format(search_terms))
    print('descr format: {0}'.format(descr))
    
    features = minDistancePairedLists(search_terms, descr)
    print('\nfeatures after minDistanceListPaired:{0}'.format(features))
    
    #%% Test Accuracy Measure
    
    p = [0,0,1,1,1,0,0]
    l = [0,0,1,0,1,0,1]
    a = accuracy(p, l)
    print('accuracy:{0}'.format(a))
    
    #%% Test Error Measure
    
    p = [0, 0.5, 1]
    l = [0, 0, 0]
    e = error(p,l)
    print('error:{0}'.format(e))
    
    #%% Test RF Classifier
    
    features = (numpy.random.rand(100))
    print('\nfeatures:{0}'.format(features))
    labels = (features<0.5).astype(int)
    print('\nlabels:{0}'.format(labels))
    clf = trainRF(features, labels)
    predictions = testRF(clf, features).round()
    print('\npredictions:{0}'.format(predictions))
    print('\naccuracy:{0}'.format(accuracy(predictions, labels)))
    print('error:{0}'.format(error(predictions, labels)))