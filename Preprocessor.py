# -*- coding: utf-8 -*-
"""

Pre-processes the data
Includes:
Normalization of the texts into a consistent format
Spellchecking
Lemmatization

Created on Sun Mar 20 21:03:09 2016

@author: H.G. van den Boorn
"""
import pickle
import Lemmatizer
import tokenizer
import time, datetime
import re
import sys

class Preprocessor:

    """
        Handles the preprocessing of the user query, and product titles, descriptions and attributes
    """

    def __init__(self):
        self.lemmatizer = Lemmatizer.Lemmatizer()

    def process_string(self, x):
        y = self.lemmatizer.get_lemmas(tokenizer.create_tokens(self.normalize_string(x)))
        return y
    
    def process_string_spellcheck(self, x):
        y = self.lemmatizer.get_lemmas_spellcheck(tokenizer.create_tokens(self.normalize_string(x)))
        return y

    def normalize_string(self, s):
        """
            Normalizes strings into a consistent format
            Example: 1.3foot, 3feet, 5ft all become xft.
        """
        nums = {'zero':0,'one':1,'two':2,'three':3,'four':4,'five':5,'six':6,'seven':7,'eight':8,'nine':9,}
        if isinstance(s, str):
            s = re.sub(r"(\w)\.([A-Z])", r"\1 \2", s) #Split words with a.A
            s = re.sub(r"([a-z])([A-Z])", r"\1 \2", s) #Split words with aA
            s = s.lower()
            s = s.replace(","," ") #could be number / segment later
            s = s.replace("$"," ")
            s = s.replace("?"," ")
            s = s.replace("!"," ")
            s = s.replace("-"," ")
            s = s.replace("#"," ")
            s = s.replace("/"," ")
            s = s.replace(")"," ")
            s = s.replace("("," ")
            s = s.replace("//","/")
            s = s.replace("..",".")
            s = s.replace(" / "," ")
            s = s.replace(" \\ "," ")
            s = s.replace("."," . ")
            s = s.replace("  "," ")
            s = re.sub(r"(^\.|/)", r"", s)
            s = re.sub(r"(\.|/)$", r"", s)
            s = re.sub(r"([0-9])([a-z])", r"\1 \2", s)
            s = re.sub(r"([a-z])([0-9])", r"\1 \2", s)
            s = s.replace(" x "," xbi ")
            s = re.sub(r"([a-z])( *)\.( *)([a-z])", r"\1 \4", s)
            s = re.sub(r"([a-z])( *)/( *)([a-z])", r"\1 \4", s)
            s = s.replace("*"," xbi ")
            s = s.replace(" by "," xbi ")
            s = re.sub(r"([0-9]+)\s*(\,+|\.+|\s+)\s*([0-9]{1,2})", r"\1.\3", s)
            s = re.sub(r"([0-9]*\.*)([0-9]+)( *)(inches|inch|in|')\.?", r"\1\2 \1\2in. ", s)
            s = re.sub(r"([0-9]*\.*)([0-9]+)( *)(foot|feet|ft|'')\.?", r"\1\2 \1\2ft. ", s)
            s = re.sub(r"([0-9]*\.*)([0-9]+)( *)(pounds|pound|lbs|lb)\.?", r"\1\2 \1\2lb. ", s)
            s = re.sub(r"([0-9]*\.*)([0-9]+)( *)(square|sq) ?\.?(feet|foot|ft)\.?", r"\1\2 \1\2sq.ft. ", s)
            s = re.sub(r"([0-9]*\.*)([0-9]+)( *)(cubic|cu) ?\.?(feet|foot|ft)\.?", r"\1\2 \1\2cu.ft. ", s)
            s = re.sub(r"([0-9]*\.*)([0-9]+)( *)(gallons|gallon|gal)\.?", r"\1\2 \1\2gal. ", s)
            s = re.sub(r"([0-9]*\.*)([0-9]+)( *)(ounces|ounce|oz)\.?", r"\1\2 \1\2oz. ", s)
            s = re.sub(r"([0-9]*\.*)([0-9]+)( *)(centimeters|cm)\.?", r"\1\2 \1\2cm. ", s)
            s = re.sub(r"([0-9]*\.*)([0-9]+)( *)(milimeters|mm)\.?", r"\1\2 \1\2mm. ", s)
            s = s.replace("°"," degrees ")
            s = re.sub(r"([0-9]*\.*)([0-9]+)( *)(degrees|degree)\.?", r"\1\2 \1\2deg. ", s)
            s = s.replace(" v "," volts ")
            s = re.sub(r"&amp",r"and",s)
            s = re.sub(r"([0-9]*\.*)([0-9]+)( *)(volts|volt)\.?", r"\1\2 \1\2volt. ", s)
            s = re.sub(r"([0-9]*\.*)([0-9]+)( *)(watts|watt)\.?", r"\1\2 \1\2watt. ", s)
            s = re.sub(r"([0-9]*\.*)([0-9]+)( *)(amperes|ampere|amps|amp)\.?", r"\1\2 \1\2amp. ", s)
            s = s.replace("  "," ")
            s = s.replace(" . "," ")
        
            for i,num in enumerate(nums):
                s = s.replace(num,str(nums[num]))
            s = re.sub(r"[^a-z\d\s\-\.]", r'', s)
    
            return s
        else:
            return "null"
    
    def processProducts(self, prod_dict):    
        prodKeys = prod_dict.keys()
        count = 0
        for prodKey in prodKeys:
            count += 1
            if count % 1000 == 0:
                print(count)
            product = prod_dict[prodKey]
            attributesList = product['attributes']
            if attributesList is not None:
                keys = list(attributesList.keys())
                for i in range(0, len(keys)):
                    attributeName = keys[i]
                    if 'bullet' in attributeName.lower():
                        del(attributesList[attributeName])
                    else:
                        valueLemmas = self.process_string(attributesList[attributeName])
                        attributesList[attributeName] = valueLemmas
            prod_desc = self.process_string(product['product_description'])
            product['attributes'] = attributesList
            product['product_description'] = prod_desc
            prod_dict[prodKey] = product
    
    def processSet(self, query):
        # assuming that the product title has no spelling mistakes, only check the user query for spelling errors
        query['search_term'] = self.process_string_spellcheck(query['search_term'])
        query['product_title'] = self.process_string(query['product_title'])
    
    def preprocess(self):
        print('Reading all the files...',end='')
        df_train = pickle.load(open('./dataset/train_set.pkl', 'rb'))
        df_test = pickle.load(open('./dataset/test_set.pkl', 'rb'))
        df_products = pickle.load(open('./dataset/products.pkl', 'rb'))
        print('Done.')
    
        starttime = time.time()
        print('Started pipeline @ '+str(datetime.datetime.now()))
        print('Processing the train set...',end='')
        sys.stdout.flush()
        for q in df_train:
            self.processSet(q)
        print('Done.')
        print('Processing the test set...',end='')
        sys.stdout.flush()
        for q in df_test:
            self.processSet(q)
        print('Done.')    
        print('Processing the products...',end='')
        sys.stdout.flush()
        self.processProducts(df_products)
        print('Done.')
        
        endtime = time.time()
        print('Duration: {0} sec.'.format(endtime - starttime))
        sys.stdout.flush()
    
        print('Saving...',end='')
        with open('./dataset/train_set_preproc.pkl', 'wb') as f:
            pickle.dump(df_train, f, pickle.HIGHEST_PROTOCOL)
        with open('./dataset/test_set_preproc.pkl', 'wb') as f:
            pickle.dump(df_test, f, pickle.HIGHEST_PROTOCOL)
        with open('./dataset/products_preproc.pkl', 'wb') as f:
            pickle.dump(df_products, f, pickle.HIGHEST_PROTOCOL)
        print('Done.')
        #Duration: 9590.163714647293 sec.
        
if __name__ == '__main__':
    preproc = Preprocessor()
    preproc.preprocess()