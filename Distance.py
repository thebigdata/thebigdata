# -*- coding: utf-8 -*-
"""
Distance

Functions for calculating distances between words and collections of words.

Has the following functions:

d = distance(w1, w2) :
calculate distance between two words as per the edit distance. 
Example: w1 = 'black', w2 = 'brown', returns 4 (4 letters have to be edited to go from one word to the other)

md = minDistance(w1, wl) : 
calculate the minimal distance between the word and any other word in the wordlist
Example: w1 = 'coffee', wl = ['brown', 'coffee'], returns 0

ml = minDistanceList(wl1, wl2)
given two word lists (wl1, wl2): for each word in wl1, calculate the minimal distances between that word and any other word in the second wordlist
example: wl1 = ['black','coffee']; wl2 = ['brown','coffee'], returns [4,0]

ml = minDistancePairedLists(wl1, wl2)
given two word list collections (wl1 and wl2), calculate the minimal distances for each pair of word lists
example: wl1 = [['coffee','black'],['door','hinge']], wl2 = [['brown','caffee'],['door','hange']], returns [[1,4],[0,1]]

Created on Mon Feb 29 12:11:13 2016

@author: Tim
"""

#%% Imports

from nltk.metrics import distance as nltkdist # for distance measure
import numpy # for constructing numpy arrays

#%% Function Definitions

"""

    DISTANCE MEASURES

"""

def distance(w1, w2):
    '''     
        DISTANCE
    
        d = distance(w1, w2)    
    
        Given two words w1 and w2,
        calculate distance between two words as per the edit distance
        returns said distance d
    
         example:
         w1 might be a word in the search term, and w2 might be a word from the title
         w1 = 'black', w2 = 'brown', returns 4 (4 letters have to be edited to go from one word to the other)
         w1 = 'coffee', w2 = 'coffee', returns 0 (0 letters have to be edited to make the two words equal)
    
    '''
    
    if w1 is None or w2 is None:
        raise TypeError('For distance, w1 and w2 can not be None. w1 is: {0} and w2 is: {1}'.format(w1, w2))
    
    if len(w1) != 3 or len(w2) != 3:
        raise ValueError('For distance, w1 and w2 must be a tuple of 3 elements: (string, type, index) of types: (str, str, int). w1 is:{0} and w2 is:{1}'.format(str(w1), str(w2)))
    
    # get info for word 1
    w1stem = w1[0]
    w1type = w1[1]    
    w1ind  = w1[2]
    
    # get info for word 2
    w2stem = w2[0]
    w2type = w2[1]
    w2ind  = w2[2]
    
    if type(w1stem) is not str or type(w1type) is not str or type(w1ind) is not int or type(w2stem) is not str or type(w2type) is not str or type(w2ind) is not int:
        raise ValueError('For distance, w1 and w2 must be a tuple of 3 elements: (string, type, index) of types: (str, str, int). w1 is:{0} and w2 is:{1}'.format(str(w1), str(w2)))
    
    dist = 0 # equal
    dist = nltkdist.edit_distance(w1stem, w2stem)
    # do fancy stuff with type and index here???
    
    return dist
    
def minDistance(w1, wl):
    '''     
        MIN DISTANCE
    
        md = minDistance(w1, wl)
        
        given a word (w1) and a list of words (wl), 
        calculate the minimal distance between the word and any other word in the wordlist
        returns said minimal distance md
        
        example:
        w1 might be a word in the search term, and wl might be the entire title
        w1 = 'coffee', wl = ['brown', 'coffee']
        returns 0
        
    '''
    
    if w1 is None or wl is None:
        raise TypeError('For minDistance, w1 must be non-None, and word list wl must be a non-None non-empty list. w1 is:{0} and wl is:{1}'.format(w1, wl))
    if len(wl) == 0:
        raise ValueError('For minDistance, w1 must be non-None, and word list wl must be a non-None non-empty list. w1 is:{0} and wl is:{1}, and length of wl is: {2}'.format(w1, wl, len(wl)))
    
    return min([distance(w1,w2) for w2 in wl])
    
def minDistanceList(wl1, wl2):
    '''    
        MIN DISTANCE LIST
        
        ml = minDistanceList(wl1, wl2)
        
        given two word lists (wl1, wl2): for each word in wl1, 
        calculate the minimal distance between that word and any other word in the second wordlist
        returns said list of minimal distances ml
         
        example: 
        wl1 might be the search query, and wl2 might be the title
        wl1 = ['black','coffee']; wl2 = ['brown','coffee']
        returns [4,0]
        
     '''
    
    if wl1 is None or wl2 is None:   
        raise TypeError('For MinDistanceList, wl1 and wl2 can not be None or empty. wl1 is:{0} and wl2 is:{1}'.format(wl1, wl2))        
    if len(wl1) == 0 or len(wl2) == 0: 
        raise ValueError('For MinDistanceList, wl1 and wl2 may not be empty. wl1 is:{0} and wl2 is:{1}, and their lengths are:{2}, {3}'.format(wl1, wl2, len(wl1), len(wl2)))  
    
    return numpy.array([minDistance(w1, wl2) for w1 in wl1])
    
def minDistancePairedLists(wl1, wl2):
    '''
        MIN DISTANCE PAIRED LISTS
        
        ml = minDistancePairedLists(wl1, wl2)
        
        given two word list collections (wl1 and wl2),
        calculate the minimal distances for each pair of word lists
        returns the list of minimal distances ml
        
        example: 
        wl1 might be a collection of search terms, and wl2 might be the product titles matched to those
        wl1 = [['coffee','black'],['door','hinge']], wl2 = [['brown','caffee'],['door','hange']]
        returns [[1,4],[0,1]]
        
    '''
    
    if wl1 is None or wl2 is None:   
        raise TypeError('For minDistancePairedLists, wl1 and wl2 can not be None (or empty, and must be of the same length). wl1 is:{0} and wl2 is:{1}'.format(wl1, wl2))   
    if len(wl1) == 0 or len(wl2) == 0: 
        raise ValueError('For minDistancePairedLists, wl1 and wl2 may not be empty (or None, and must be of the same length). wl1 is:{0} and wl2 is:{1}, and their lengths are:{2}, {3}'.format(wl1, wl2, len(wl1), len(wl2)))
    if len(wl1) is not len(wl2):
        raise ValueError('For minDistancePairedLists, the length of wl1 and wl2 must be the same (as well as non-empty, non-None). wl1 is:{0} and wl2 is:{1}, and their lengths are: {2} and {3}'.format(wl1, wl2, len(wl1), len(wl2)))  
    
    return numpy.array([minDistanceList(wl1[i], wl2[i]) for i in range(0,len(wl1))])

#%% Test Cases
    
'''

    TEST CASES

'''

if __name__ == '__main__': 
    
    #%% Test Distance
        
    w1 = ('hallo', 'N', 1)
    w2 = ('halal', 'N', 1)
    
    dist = distance(w1, w2)
    print('distance:{0}'.format(dist))
    
    #%% Test min distance with multiple words
    
    wl = ['hallo', 'hello', 'helel']
    wl = [(w, 'N', 1) for w in wl]
    dist = minDistance(w1, wl)
    print('distance after minDistance:{0}'.format(dist))
    
    #%% Test min distance with multiple words lists
    
    dist = minDistanceList([w1,w2], wl)
    print('distance after minDistanceList:{0}'.format(dist))
    
    #%% Test Distance for multiple paired words lists
            
    search_terms = [['blue','blobs'],['doo', 'brown'],['hulu','subscription'],['holla','back','gurllll'],['halal'],['one','of','those','things','that','go','ding','dong']]
    descr = [['bla','blob','blue'],['herpy','derpy','doo'],['hallo','hulu','hodor'],['hello','holla','yubnub'],['hallo','halal'],['boom','shaka','laka','ding','dong']]
    search_terms = [[(w, 'N', 1) for w in d] for d in search_terms]
    descr = [[(w, 'N', 1) for w in d] for d in descr]
    print('\nsearch terms format: {0}'.format(search_terms))
    print('descr format: {0}'.format(descr))
    
    features = minDistancePairedLists(search_terms, descr)
    print('\nfeatures after minDistanceListPaired:{0}'.format(features))