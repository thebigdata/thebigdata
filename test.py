# -*- coding: utf-8 -*-
"""
Created on Thu Mar 17 14:48:54 2016

@author: H.G. van den Boorn
"""

from Lemmatizer import Lemmatizer
from Similarity_word2vec import Similarity
import pickle
import tokenizer

products_file = open('./dataset/products.pkl', 'rb')
products = pickle.load(products_file)
train_file = open('./dataset/train_set.pkl', 'rb')
train = pickle.load(train_file)

lt = Lemmatizer()
print('Loading word2vec...')
s = Similarity()
print('Done! :)')

#%%
pd = lt.get_lemmas(tokenizer.create_tokens(products[100001]['product_description']))
search = lt.get_lemmas(tokenizer.create_tokens(train[0]['search_term']))

st = s.get_w2v_stats(search,pd,'N V A'.split())