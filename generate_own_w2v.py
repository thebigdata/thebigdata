# -*- coding: utf-8 -*-
"""

Creates our own word2vec representation for use in feature generation.

Created on Tue Mar 29 20:02:26 2016

@author: H&W
"""
from nltk.stem import WordNetLemmatizer
from nltk.corpus import wordnet
import corrector
from nltk import distance as nltkdist # for distance measure
import nltk
import pickle
import gensim
import re
import numpy as np
import pickle
from gensim.models import word2vec
from gensim.models.word2vec import Word2Vec
import csv
import Preprocessor
PRODUCTS = []
with open('dataset/product_descriptions.csv', 'r', encoding='unicode_escape') as csvfile:
    d = csv.DictReader(csvfile)
    for row in d:
        PRODUCTS.append(row['product_description'])
        
def get_wordnet_pos(treebank_tag):
    if treebank_tag.startswith('J'):
        return wordnet.ADJ
    elif treebank_tag.startswith('V'):
        return wordnet.VERB
    elif treebank_tag.startswith('N'):
        return wordnet.NOUN
    elif treebank_tag.startswith('R'):
        return wordnet.ADV
    else:
        return wordnet.NOUN
def hash32(value):     
    return hash(value) & 0xffffffff
    
def normalize_string(y):
    nums = {'zero':0,'one':1,'two':2,'three':3,'four':4,'five':5,'six':6,'seven':7,'eight':8,'nine':9,}
    new = []
    lemmas = []
    count = 0
    WL = WordNetLemmatizer()
    for s in y:
        count += 1
        if count % 1000 == 0:
            print(count)
        s = re.sub(r"(\w)\.([A-Z])", r"\1 \2", s) #Split words with a.A
        s = re.sub(r"([a-z])([A-Z])", r"\1 \2", s) #Split words with aA
        s = s.lower()
        s = s.replace(","," ") #could be number / segment later
        s = s.replace("$"," ")
        s = s.replace("?"," ?")
        s = s.replace("!"," !")
        s = s.replace("-"," ")
        s = s.replace("#"," ")
        s = s.replace("/"," ")
        s = s.replace(")"," ")
        s = s.replace("("," ")
        s = s.replace("//","/")
        s = s.replace("..",".")
        s = s.replace(" / "," ")
        s = s.replace(" \\ "," ")
        s = s.replace("."," . ")
        s = re.split(r" [\.|\?|\!]", s)
        for x in s:
            x = x.replace("?"," ")
            x = x.replace("!"," ")
            x = x.replace("  "," ")
            x = re.sub(r"(^\.|/)", r"", x)
            x = re.sub(r"(\.|/)$", r"", x)
            x = re.sub(r"([0-9])([a-z])", r"\1 \2", x)
            x = re.sub(r"([a-z])([0-9])", r"\1 \2", x)
            x = x.replace(" x "," xbi ")
            x = re.sub(r"([a-z])( *)\.( *)([a-z])", r"\1 \4", x)
            x = re.sub(r"([a-z])( *)/( *)([a-z])", r"\1 \4", x)
            x = x.replace("*"," xbi ")
            x = x.replace(" by "," xbi ")
            x = re.sub(r"([0-9]+)\s*(\,+|\.+|\s+)\s*([0-9]{1,2})", r"\1.\3", x)
            x = re.sub(r"([0-9]*\.*)([0-9]+)( *)(inches|inch|in|')\.?", r"\1\2 \1\2in. ", x)
            x = re.sub(r"([0-9]*\.*)([0-9]+)( *)(foot|feet|ft|'')\.?", r"\1\2 \1\2ft. ", x)
            x = re.sub(r"([0-9]*\.*)([0-9]+)( *)(pounds|pound|lbs|lb)\.?", r"\1\2 \1\2lb. ", x)
            x = re.sub(r"([0-9]*\.*)([0-9]+)( *)(square|sq) ?\.?(feet|foot|ft)\.?", r"\1\2 \1\2sq.ft. ", x)
            x = re.sub(r"([0-9]*\.*)([0-9]+)( *)(cubic|cu) ?\.?(feet|foot|ft)\.?", r"\1\2 \1\2cu.ft. ", x)
            x = re.sub(r"([0-9]*\.*)([0-9]+)( *)(gallons|gallon|gal)\.?", r"\1\2 \1\2gal. ", x)
            x = re.sub(r"([0-9]*\.*)([0-9]+)( *)(ounces|ounce|oz)\.?", r"\1\2 \1\2oz. ", x)
            x = re.sub(r"([0-9]*\.*)([0-9]+)( *)(centimeters|cm)\.?", r"\1\2 \1\2cm. ", x)
            x = re.sub(r"([0-9]*\.*)([0-9]+)( *)(milimeters|mm)\.?", r"\1\2 \1\2mm. ", x)
            x = x.replace("°"," degrees ")
            x = re.sub(r"([0-9]*\.*)([0-9]+)( *)(degrees|degree)\.?", r"\1\2 \1\2deg. ", x)
            x = x.replace(" v "," volts ")
            x = re.sub(r"([0-9]*\.*)([0-9]+)( *)(volts|volt)\.?", r"\1\2 \1\2volt. ", x)
            x = re.sub(r"([0-9]*\.*)([0-9]+)( *)(watts|watt)\.?", r"\1\2 \1\2watt. ", x)
            x = re.sub(r"([0-9]*\.*)([0-9]+)( *)(amperes|ampere|amps|amp)\.?", r"\1\2 \1\2amp. ", x)
            x = x.replace("  "," ")            
            x = re.sub(r"[^a-z\d\s\-\.]", r'', x)
            for i,num in enumerate(nums):
                x = x.replace(num,str(nums[num]))
            x = x.split()
            PT = nltk.pos_tag(x)
            lemmas = [(WL.lemmatize(tag[0], get_wordnet_pos(tag[1]))) for index, tag in enumerate(PT)]
            new.append(lemmas)
    return new
    
PRODUCTS_new = normalize_string(PRODUCTS)
print('Done preproc')
with open('dataset/allsentences.pkl', 'wb') as f:
    pickle.dump(PRODUCTS_new, f, pickle.HIGHEST_PROTOCOL)
model = Word2Vec(PRODUCTS_new, size=100, window=5, min_count=5, workers=4, hashfxn=hash32)
print('done all')
model.save('dataset/own_w2v_model')