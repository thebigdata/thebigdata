import numpy as np
import pandas as pd
import sys
import time
import pickle 
import re
import multiprocessing
from sklearn.svm import SVR
from sklearn.ensemble import RandomForestRegressor, BaggingRegressor
from nltk.stem.snowball import SnowballStemmer
from pipeline import stem
import psutil



#def str_stemmer(s):
#	return " ".join([stemmer.stem(word) for word in s.lower().split()])

def str_common_word(str1, str2):
	return sum(int(str2.find(word)>=0) for word in str1.split())
 
def str_bigram(str1, str2):
    count = 0
    for gram1 in ngrams(str1,2):
        for gram2 in ngrams(str2,2):
            if gram1 == gram2:
                count += 1
    return count
 
def str_trigram(str1, str2):
    count = 0
    for gram1 in ngrams(str1,3):
        for gram2 in ngrams(str2,3):
            if gram1 == gram2:
                count += 1
    return count
    
def tupel2str(listoftupel):
    tmplst = list()
    for tupel in listoftupel:
        tmplst.append(tupel[0])
    return ' '.join(tmplst)
 
def findWholeWord(w):
    return re.compile(r'\b({0})\b'.format(w), flags=re.IGNORECASE).search
    
def ngrams(input, n):
    input = input.split(' ')
    output = []
    for i in range(len(input)-n+1):
      output.append(input[i:i+n])
    return output
if __name__ == '__main__':
    
    n_workers = multiprocessing.cpu_count() - 0                                # change to -1 if you don't want to use all cores
    print('Starting pool with ' + str(n_workers) + ' workers')    
    pool = multiprocessing.Pool(n_workers)
    parent = psutil.Process()
    parent.nice(psutil.IDLE_PRIORITY_CLASS)
    for child in parent.children():                                            # setting priority to lowest, otherwise your PC becomes a slug
        child.nice(psutil.IDLE_PRIORITY_CLASS)
    stemmer = SnowballStemmer('english')
    
    tic = time.time()
    df_train = pd.read_csv('./dataset/train.csv', encoding="ISO-8859-1")
    df_test = pd.read_csv('./dataset/test.csv', encoding="ISO-8859-1")
    #df_attr = pd.read_csv('../input/attributes.csv')
    df_pro_desc = pd.read_csv('./dataset/product_descriptions.csv')
    products = pickle.load(open('./dataset/products.pkl', 'rb'))
    
    print('data loaded')
    
    num_train = df_train.shape[0]
    
    df_all = pd.concat((df_train, df_test), axis=0, ignore_index=True)
    df_all = pd.merge(df_all, df_pro_desc, how='left', on='product_uid')
    
    print('brand name vector')
    
    brand_in_search = np.zeros(len(df_all), dtype=bool)
    has_attribute = np.zeros(len(df_all),dtype=bool)
    
    for i in range(len(df_all)):
        uid = df_all['product_uid'][i]
        brand = "N/A"
        if products[uid]['attributes'] != None:
            has_attribute[i] = 1
            if 'MFG Brand Name' in products[uid]['attributes']:
                brand = products[uid]['attributes']['MFG Brand Name'].lower()
        if findWholeWord(brand)(df_all['search_term'][i]):
    #        if i%10000 ==0: print (['yes: ',i, brand])
            brand_in_search[i] = 1
            
    df_all['has_attributes'] = has_attribute
    print('start stemming')
    sys.stdout.flush()
    print('1/3')
    df_all['search_term'] = pool.map(stem, df_all['search_term'])
    print('2/3')
    df_all['product_title'] = pool.map(stem, df_all['product_title'])
    print('3/3')
    df_all['product_description'] = pool.map(stem, df_all['product_description'])
    print('formatting')
    sys.stdout.flush()

    df_all['search_term'] = pool.map(tupel2str, df_all['search_term'])
    df_all['product_description'] = pool.map(tupel2str, df_all['product_description'])
    df_all['product_title'] = pool.map(tupel2str, df_all['product_title'])
    pool.close()
    df_all['len_of_query'] = df_all['search_term'].map(lambda x:len(x.split())).astype(np.int64)
    
    print('search ngrams')
    sys.stdout.flush()
    bigrams = np.zeros(len(df_all), dtype=int)
    trigrams = np.zeros(len(df_all), dtype=int)
    bigrams_title = np.zeros(len(df_all), dtype=int)
    trigrams_title = np.zeros(len(df_all), dtype=int)
    
    for i in range(len(df_all)):
    #    if i%10000==0: print(i)
        bigrams[i]  = str_bigram(df_all['search_term'][i], df_all['product_description'][i])
        trigrams[i] = str_trigram(df_all['search_term'][i], df_all['product_description'][i])
        bigrams_title[i] = str_bigram(df_all['search_term'][i], df_all['product_title'][i])
        trigrams_title[i] = str_trigram(df_all['search_term'][i], df_all['product_title'][i])
    df_all['bigrams'] = bigrams
    df_all['trigrams'] = trigrams
    df_all['bigrams_title'] = bigrams_title
    df_all['trigrams_title'] = trigrams_title
    
    print('start mapping')
    sys.stdout.flush()
    df_all['product_info'] = df_all['search_term']+"\t"+df_all['product_title']+"\t"+df_all['product_description']
    print('1/3')
    df_all['word_in_title'] = df_all['product_info'].map(lambda x:str_common_word(x.split('\t')[0],x.split('\t')[1]))
    print('2/3')
    df_all['word_in_description'] = df_all['product_info'].map(lambda x:str_common_word(x.split('\t')[0],x.split('\t')[2]))
    
    df_all['word_in_title'] = df_all['word_in_title'] / df_all['len_of_query']
    df_all['word_in_description'] = df_all['word_in_description'] / df_all['len_of_query']
    
    
    df_all = df_all.drop(['search_term','product_title','product_description','product_info'],axis=1)
    df_all['brand_in_search'] = brand_in_search    
    print('start preparation')
    sys.stdout.flush()
    df_train = df_all.iloc[:num_train]
    df_test = df_all.iloc[num_train:]
    id_test = df_test['id']
    
    y_train = df_train['relevance'].values
    X_train = df_train.drop(['id','relevance'],axis=1).values
    X_test = df_test.drop(['id','relevance'],axis=1).values
    
    rf = RandomForestRegressor(n_estimators=20, max_depth=7, random_state=0)
    clf = BaggingRegressor(rf, n_estimators=50, max_samples=0.15, random_state=25)
    
    print('train..')
    sys.stdout.flush()
    clf.fit(X_train, y_train)
    y_pred = clf.predict(X_test)
    
#    clf = SVR(C=1.0, epsilon=0.2)
#    clf.fit(X_train, y_train)
#    y_pred = clf.predict(X_test) 
#    
    pd.DataFrame({"id": id_test, "relevance": y_pred}).to_csv('submission.csv',index=False)
    
    print([ (time.time()-tic)/60 , ' Minutes '])